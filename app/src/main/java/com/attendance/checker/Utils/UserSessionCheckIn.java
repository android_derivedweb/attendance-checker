package com.attendance.checker.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSessionCheckIn {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "UserSessionPrefCheckIn";

    private static final String CHECKIN = "CHECKIN";
    private static final String USER_ID = "USER_ID";
    private static final String EMP_ISCHECK = "EMP_ISCHECK";



    public UserSessionCheckIn(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void setCheckin(String checkin){
        editor.putString(CHECKIN, checkin);
        editor.commit();
    }

    public String getCheckin() {
        return sharedPreferences.getString(CHECKIN, "");
    }


   public void setUserId(String userId){
        editor.putString(USER_ID, userId);
        editor.commit();
    }

    public String getUserId() {
        return sharedPreferences.getString(USER_ID, "0");
    }


    public void setIsCheckIn(boolean isCheckIn){
        editor.putBoolean(EMP_ISCHECK, isCheckIn);
        editor.commit();
    }

    public boolean getEmpIscheck() {
        return sharedPreferences.getBoolean(EMP_ISCHECK, false);
    }


    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }


}
