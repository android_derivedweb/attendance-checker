package com.attendance.checker.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

public class UserSession {

    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    private static final String PREF_NAME = "UserSessionPref";

    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String EMP_ID = "EMP_ID";
    private static final String EMP_NAME = "EMP_NAME";
    private static final String EMP_EMAIL = "EMP_EMAIL";
    private static final String EMP_NUMBER = "EMP_NUMBER";
    private static final String EMP_LAST_NAME = "EMP_LAST_NAME";
    private static final String USERTYPE = "USERTYPE";


    public static final String BASEURL = "https://script.google.com/macros/s/AKfycbxOLElujQcy1-ZUer1KgEvK16gkTLUqYftApjNCM_IRTL3HSuDk/exec?id=1vw6sf6UWHlqCFc6drQhJb1uwos1aZ9fXtJRGZ1ZpBLw&sheet=";
    public static final String ADD_Admin = "https://script.google.com/macros/s/AKfycbzMTJYzyJ4Rm6X4XDgwy7cSEQOWGd3QCZJwKOJ7U8bVHQtK3ClBrh5NF8KUNfmIaTbV4A/exec";
    public static final String ADD_Employees = "https://script.google.com/macros/s/AKfycbzxONh7ofifh4KHjBeHObwwVpOJF7DLcnAwBHJqEGTCsXJI2JbZENzykIrFEudm9Vl8/exec";
    public static final String ADD_Leaves = "https://script.google.com/macros/s/AKfycbxV4T3IPkDc23R2rCAdWXTlaauc3rzvWTBYQB5nZ6x2qeBz7CyLXQsDm3Au-GG-JC9h/exec";
    public static final String ADD_CheckIn = "https://script.google.com/macros/s/AKfycbxfTgqXfl3h1PyqGaT3rmnCt6ry1kyHPpyLy-E-rE-HfRyzsqbwXZ8MMvlKr39U9DYW/exec";
    public static final String UPDDATE_LEAVE = "https://script.google.com/macros/s/AKfycbxDgayyGRiXXwSluWSN5coI_3GPjEKm3PGZWI6Q8FuVuTVJMj-X-9bO7sdspIH60jco/exec";
    public static final String UPDDATE_PASSWORD = "https://script.google.com/macros/s/AKfycbxDHqeNJEbBO1nFMA55hJ5vl4fTkQ7V9WAlIy8CeepPsLsa_0MQMaa9dXvcL6OTdrpqyA/exec";
    public static final String UPDDATE_LEAVEDAYS = "https://script.google.com/macros/s/AKfycbxbzY7ajPIkdPZAfU_ocWKSFHD-GLRnMnBJeV8RxKppv-DSXS9JUhEddYSuGDuJTcEO2w/exec";
    public static final String UPDATE_MOBILE = "https://script.google.com/macros/s/AKfycbxoMEapbwsp63i47SNy_dCHuYoaBBJc4z70AuvDfm0Grf4_klCIRRACjtMb3sPp8cvk4w/exec";




    public UserSession(Context context) {
        this.context = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public void setIsLogin(boolean isLogin){
        editor.putBoolean(IS_LOGIN, isLogin);
        editor.commit();
    }

    public boolean logout() {
        return sharedPreferences.edit().clear().commit();
    }

    public void createLoginSession(String mEMP_ID,
                                   String mEMP_NAME,
                                   String mEMP_L_NAME,
                                   String mEMP_EMAIL,
                                   String mEMP_NUMBER){

        editor.putString(EMP_ID, mEMP_ID);
        editor.putString(EMP_NAME, mEMP_NAME);
        editor.putString(EMP_LAST_NAME, mEMP_L_NAME);
        editor.putString(EMP_EMAIL, mEMP_EMAIL);
        editor.putString(EMP_NUMBER, mEMP_NUMBER);
        editor.commit();

    }

    public String getEmpId() {
        return sharedPreferences.getString(EMP_ID, "");
    }

    public String getEmpName() {
        return sharedPreferences.getString(EMP_NAME, "");
    }

    public String getEmpLastName() {
        return sharedPreferences.getString(EMP_LAST_NAME, "");
    }

    public String getEmpEmail() {
        return sharedPreferences.getString(EMP_EMAIL, "");
    }

    public String getEmpNumber() {
        return sharedPreferences.getString(EMP_NUMBER, "");
    }


    public void setUsertype(String usertype){
        editor.putString(USERTYPE, usertype);
        editor.commit();
    }


    public String getUsertype() {
        return sharedPreferences.getString(USERTYPE, "");
    }



}
