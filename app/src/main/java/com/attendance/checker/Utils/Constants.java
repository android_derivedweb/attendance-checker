package com.attendance.checker.Utils;

public class Constants {
    public static final String PACKAGE_NAME = "com.attendance.checker";
    static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String RECEVIER = PACKAGE_NAME + ".RECEVIER";
    static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    static final String ADDRESS = PACKAGE_NAME + ".ADDRESS";
    static final String LOCAITY = PACKAGE_NAME + ".LOCAITY";
    static final String COUNTRY = PACKAGE_NAME + ".COUNTRY";
    static final String DISTRICT = PACKAGE_NAME + ".DISTRICT";
    static final String POST_CODE = PACKAGE_NAME + ".POST_CODE";
    static final String STATE = PACKAGE_NAME + ".STATE";
    public static final String LATITUDE = "25.2516";
    public static final String LONGITUDE = "55.2953";
    public static final String range = "2000000";

    static final int SUCCESS_RESULT = 1;
    static final int FAILURE_RESULT = 0;
}
