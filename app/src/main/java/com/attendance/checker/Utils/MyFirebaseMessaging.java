package com.attendance.checker.Utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;

import com.attendance.checker.Activity.User.User_MainActivity;
import com.attendance.checker.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONObject;

import java.util.Map;

public class MyFirebaseMessaging extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;
    private RemoteViews contentView;


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);

        Log.e("newToken_3", token);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        JSONObject jsonObject = new JSONObject(remoteMessage.getData());
        Log.e("sdsd",jsonObject.toString());

       // sendNotification(remoteMessage.getData());

        Bundle msg = new Bundle();
        for (String key : remoteMessage.getData().keySet()) {
            msg.putString(key, remoteMessage.getData().get(key));
        }

        if(msg.getString("body").contains("Approved")){
            show_Notification(msg.getString("body"),"Congrats Your Leaves Approved!!!");
        }else {
            show_Notification(msg.getString("body"),"Sorry Your Leaves Rejected!!!");
        }

    }


    int i = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void show_Notification(String text,String tilte) {

        i++;

        if(text.contains("Approved")){
            contentView = new RemoteViews(getPackageName() , R.layout.custom_notification_layout ) ;
            contentView.setTextViewText(R.id.title, tilte + "  "+ getEmojiByUnicode(0x1F60A));
          //  contentView.setTextViewText(R.id.title, tilte + "  "+ getEmojiByUnicode(0x1F48B));

        }else {
            contentView = new RemoteViews(getPackageName() , R.layout.custom_notification_layout2 ) ;
            contentView.setTextViewText(R.id.title, tilte + "  "+ getEmojiByUnicode(0x1F62A));

        }
        String[] separated = text.split("To");
        contentView.setImageViewResource(R.id.image, R.drawable.app_logo);
        contentView.setTextViewText(R.id.text, separated[0]);
        contentView.setTextViewText(R.id.text2, "To"+separated[1]);

        Intent intent = new Intent(getApplicationContext(), User_MainActivity.class);
        String CHANNEL_ID = "MYCHANNEL";


        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_HIGH);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, 0);
        Notification notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                .setContent(contentView)
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID)
                .setSmallIcon(R.drawable.app_logo)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(i, notification);

    }



    public String getEmojiByUnicode(int unicode){
        return new String(Character.toChars(unicode));
    }







}
