package com.attendance.checker.Utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.attendance.checker.Model.NotificationModel;

import java.util.ArrayList;

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "HudoorApp";

    private static final String TABLE_CART = "nofication_data";
    private static final String KEY_EMPLOYEE_ID = "employee_id";
    private static final String KEY_START_DATE = "start_date";
    private static final String KEY_END_DATE = "end_date";

    private final Context mContext;

    public Database(Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + TABLE_CART + "("
                + KEY_EMPLOYEE_ID + " TEXT ,"
                + KEY_START_DATE + " TEXT ,"
                + KEY_END_DATE + " TEXT "+ ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CART);
        onCreate(db);
    }

    public boolean InsertDetails(String employee_id, String start_date, String end_date) {

        SQLiteDatabase db = this.getWritableDatabase();

            ContentValues values = new ContentValues();
            values.put(KEY_EMPLOYEE_ID, employee_id);
            values.put(KEY_START_DATE, start_date);
            values.put(KEY_END_DATE, end_date);

            db.insert(TABLE_CART, null, values);
            db.close();


        return true;
    }

    public ArrayList<NotificationModel> getAllUser() {
        ArrayList<NotificationModel> detailsModelArrayList = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + TABLE_CART;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor =db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                NotificationModel model = new NotificationModel();
                model.setEmployee_id(cursor.getString(0));
                model.setStart_date(cursor.getString(1));
                model.setEnd_date(cursor.getString(2));
                detailsModelArrayList.add(model);
            }while (cursor.moveToNext());
        }
        return detailsModelArrayList;
    }

   /* public Boolean Update(String id, String quantity) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_QUANTITY, quantity);
        db.update(TABLE_CART, values,KEY_DEAL_OPTION_ID+"=" +id,null);
        db.close();
        return null;

    }*/

    public void Delete() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ TABLE_CART);

        db.close();
    }

    /*public Cursor findTask(String taskName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_WISHLIST + " WHERE " +
                KEY_TITLE + " = '" + taskName + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }*/
}