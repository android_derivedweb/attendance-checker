package com.attendance.checker.Utils;

import com.attendance.checker.Model.MyResponse;
import com.attendance.checker.Model.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAVemClfI:APA91bGbrPaa6WDBzGjimQ_tYqZY2_kIJiBKn1Ydbuf7j1KYiL1wSTk0dEDRQgcl6gEY1DXA-oRLQQTVjFQc8gC1Nimz-Dv4CMVSLkHYiT_FJQr3xiVX7MxLl4iPu7BUwwdWK2DrzrBh"
            })

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}