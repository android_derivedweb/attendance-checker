package com.attendance.checker.Model;

import java.util.ArrayList;

public class CityModel {

    String Cityname;
    String CityId;

    public String getCityname() {
        return Cityname;
    }

    public void setCityname(String cityname) {
        Cityname = cityname;
    }

    public String getCityId() {
        return CityId;
    }

    public void setCityId(String cityId) {
        CityId = cityId;
    }

}
