package com.attendance.checker.Model;

public class EmployeeModel {
    private String Employee_Email_ID;
    private String Employee_First_Name;
    private String Employee_ID;
    private String Employee_Last_Name;
    private String status;

    public String getEmployee_Email_ID() {
        return Employee_Email_ID;
    }

    public void setEmployee_Email_ID(String employee_Email_ID) {
        Employee_Email_ID = employee_Email_ID;
    }

    public String getEmployee_First_Name() {
        return Employee_First_Name;
    }

    public void setEmployee_First_Name(String employee_First_Name) {
        Employee_First_Name = employee_First_Name;
    }

    public String getEmployee_ID() {
        return Employee_ID;
    }

    public void setEmployee_ID(String employee_ID) {
        Employee_ID = employee_ID;
    }

    public String getEmployee_Last_Name() {
        return Employee_Last_Name;
    }

    public void setEmployee_Last_Name(String employee_Last_Name) {
        Employee_Last_Name = employee_Last_Name;
    }

    public String getEmployee_Mobile_Number() {
        return Employee_Mobile_Number;
    }

    public void setEmployee_Mobile_Number(String employee_Mobile_Number) {
        Employee_Mobile_Number = employee_Mobile_Number;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    private String Employee_Mobile_Number;
    private String Password;

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}
