package com.attendance.checker.Model;

public class LeaveBalanceModel {
    private String totalAnnual_Leave;
    private String balanceAnnual_Leave;
    private String totalSick_Leave;
    private String balanceSick_Leave;
    private String totalUnpaid_Leave;
    private String balanceUnpaid_Leave;
    private String totalMaternity_Leave;
    private String balanceMaternity_Leave;

    private String totalHAL;
    private String balanceHAL;

    private String totalHUL;
    private String balanceHUL;

    private String totalWFHL;
    private String balanceWFHL;

    private String totalSPL;
    private String balanceSPL;
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTotalHAL() {
        return totalHAL;
    }

    public void setTotalHAL(String totalHAL) {
        this.totalHAL = totalHAL;
    }

    public String getBalanceHAL() {
        return balanceHAL;
    }

    public void setBalanceHAL(String balanceHAL) {
        this.balanceHAL = balanceHAL;
    }

    public String getTotalHUL() {
        return totalHUL;
    }

    public void setTotalHUL(String totalHUL) {
        this.totalHUL = totalHUL;
    }

    public String getBalanceHUL() {
        return balanceHUL;
    }

    public void setBalanceHUL(String balanceHUL) {
        this.balanceHUL = balanceHUL;
    }

    public String getTotalWFHL() {
        return totalWFHL;
    }

    public void setTotalWFHL(String totalWFHL) {
        this.totalWFHL = totalWFHL;
    }

    public String getBalanceWFHL() {
        return balanceWFHL;
    }

    public void setBalanceWFHL(String balanceWFHL) {
        this.balanceWFHL = balanceWFHL;
    }

    public String getTotalSPL() {
        return totalSPL;
    }

    public void setTotalSPL(String totalSPL) {
        this.totalSPL = totalSPL;
    }

    public String getBalanceSPL() {
        return balanceSPL;
    }

    public void setBalanceSPL(String balanceSPL) {
        this.balanceSPL = balanceSPL;
    }

    public void setTotalAnnual_Leave(String totalAnnual_leave) {
        this.totalAnnual_Leave = totalAnnual_leave;
    }

    public String getTotalAnnual_Leave() {
        return totalAnnual_Leave;
    }

    public void setBalanceAnnual_Leave(String balanceAnnual_leave) {
        this.balanceAnnual_Leave = balanceAnnual_leave;
    }

    public String getBalanceAnnual_Leave() {
        return balanceAnnual_Leave;
    }

    public void setTotalSick_Leave(String totalSick_leave) {
        this.totalSick_Leave = totalSick_leave;
    }

    public String getTotalSick_Leave() {
        return totalSick_Leave;
    }

    public void setBalanceSick_Leave(String balanceSick_leave) {
        this.balanceSick_Leave = balanceSick_leave;
    }

    public String getBalanceSick_Leave() {
        return balanceSick_Leave;
    }

    public void setTotalUnpaid_Leave(String totalUnpaid_leave) {
        this.totalUnpaid_Leave = totalUnpaid_leave;
    }

    public String getTotalUnpaid_Leave() {
        return totalUnpaid_Leave;
    }

    public void setBalanceUnpaid_Leave(String balanceUnpaid_leave) {
        this.balanceUnpaid_Leave = balanceUnpaid_leave;
    }

    public String getBalanceUnpaid_Leave() {
        return balanceUnpaid_Leave;
    }

    public void setTotalMaternity_Leave(String totalMaternity_leave) {
        this.totalMaternity_Leave = totalMaternity_leave;
    }

    public String getTotalMaternity_Leave() {
        return totalMaternity_Leave;
    }

    public void setBalanceMaternity_Leave(String balanceMaternity_leave) {
        this.balanceMaternity_Leave = balanceMaternity_leave;
    }

    public String getBalanceMaternity_Leave() {
        return balanceMaternity_Leave;
    }
}
