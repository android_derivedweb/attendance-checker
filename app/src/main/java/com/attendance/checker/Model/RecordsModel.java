package com.attendance.checker.Model;

public class RecordsModel {

    String date;
    String Attendance_In_Time;
    String Attendance_Out_Time;
    String Duration_of_Time_in_Office;
    String If_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAttendance_In_Time() {
        return Attendance_In_Time;
    }

    public void setAttendance_In_Time(String attendance_In_Time) {
        Attendance_In_Time = attendance_In_Time;
    }

    public String getAttendance_Out_Time() {
        return Attendance_Out_Time;
    }

    public void setAttendance_Out_Time(String attendance_Out_Time) {
        Attendance_Out_Time = attendance_Out_Time;
    }

    public String getDuration_of_Time_in_Office() {
        return Duration_of_Time_in_Office;
    }

    public void setDuration_of_Time_in_Office(String duration_of_Time_in_Office) {
        Duration_of_Time_in_Office = duration_of_Time_in_Office;
    }

    public String getIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y() {
        return If_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y;
    }

    public void setIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y(String if_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y) {
        If_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y = if_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y;
    }
}