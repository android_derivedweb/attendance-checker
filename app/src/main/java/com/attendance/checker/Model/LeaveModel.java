package com.attendance.checker.Model;

public class LeaveModel {


    private String Date_of_Leave_Application;
    private String Employee_ID;
    private String Employee_First_Name;
    private String Employee_Last_Name;
    private String Type_of_Leave;
    private String Start_Date_of_Leave;
    private String End_Date_of_Leave;

    public String getDate_of_Leave_Application() {
        return Date_of_Leave_Application;
    }

    public void setDate_of_Leave_Application(String date_of_Leave_Application) {
        Date_of_Leave_Application = date_of_Leave_Application;
    }

    public String getEmployee_ID() {
        return Employee_ID;
    }

    public void setEmployee_ID(String employee_ID) {
        Employee_ID = employee_ID;
    }

    public String getEmployee_First_Name() {
        return Employee_First_Name;
    }

    public void setEmployee_First_Name(String employee_First_Name) {
        Employee_First_Name = employee_First_Name;
    }

    public String getEmployee_Last_Name() {
        return Employee_Last_Name;
    }

    public void setEmployee_Last_Name(String employee_Last_Name) {
        Employee_Last_Name = employee_Last_Name;
    }

    public String getType_of_Leave() {
        return Type_of_Leave;
    }

    public void setType_of_Leave(String type_of_Leave) {
        Type_of_Leave = type_of_Leave;
    }

    public String getStart_Date_of_Leave() {
        return Start_Date_of_Leave;
    }

    public void setStart_Date_of_Leave(String start_Date_of_Leave) {
        Start_Date_of_Leave = start_Date_of_Leave;
    }

    public String getEnd_Date_of_Leave() {
        return End_Date_of_Leave;
    }

    public void setEnd_Date_of_Leave(String end_Date_of_Leave) {
        End_Date_of_Leave = end_Date_of_Leave;
    }

    public String getDuration_of_Leave() {
        return Duration_of_Leave;
    }

    public void setDuration_of_Leave(String duration_of_Leave) {
        Duration_of_Leave = duration_of_Leave;
    }

    public String getBalance_Type_of_Leave_After_Application() {
        return Balance_Type_of_Leave_After_Application;
    }

    public void setBalance_Type_of_Leave_After_Application(String balance_Type_of_Leave_After_Application) {
        Balance_Type_of_Leave_After_Application = balance_Type_of_Leave_After_Application;
    }

    public String getAuthorised_by_Admin() {
        return Authorised_by_Admin;
    }

    public void setAuthorised_by_Admin(String authorised_by_Admin) {
        Authorised_by_Admin = authorised_by_Admin;
    }

    private String Duration_of_Leave;
    private String Balance_Type_of_Leave_After_Application;
    private String Authorised_by_Admin;


}
