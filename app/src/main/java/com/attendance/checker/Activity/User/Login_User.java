package com.attendance.checker.Activity.User;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.provider.Settings;
import android.text.InputType;
import android.text.TextUtils;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.UserSessionCheckIn;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Login_User extends AppCompatActivity {

    private EditText mPassword, employee_id;
    private ImageView icon_password_visible_signup, icon_password_invisible_signup;
    private TextView header_title;
    private UserSession mSession;
    private ArrayList<EmployeeModel> employeeModelArrayList = new ArrayList<>();

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private UserSessionCheckIn userSessionCheckIn;
    private String device_token;
    private FirebaseAuth firebasing;
    private DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        mSession = new UserSession(Login_User.this);
        userSessionCheckIn = new UserSessionCheckIn(Login_User.this);


        employee_id = findViewById(R.id.employee_id);
        mPassword = findViewById(R.id.mPassword);
        icon_password_visible_signup = findViewById(R.id.icon_password_visible_signup);
        icon_password_invisible_signup = findViewById(R.id.icon_password_invisible_signup);
        header_title = findViewById(R.id.header_title);

        header_title.setText("Employee Login");
        ref= FirebaseDatabase.getInstance().getReference();
        firebasing =FirebaseAuth.getInstance();


        findViewById(R.id.backToHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        icon_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_signup.setVisibility(View.GONE);
                icon_password_invisible_signup.setVisibility(View.VISIBLE);


                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_password_invisible_signup.setVisibility(View.GONE);
                icon_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });



        findViewById(R.id.btnLogIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (employee_id.getText().toString().isEmpty()) {
                    Toast.makeText(Login_User.this, "Please Your Employee ID", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Login_User.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else {

                    if (ContextCompat.checkSelfPermission(getApplicationContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(Login_User.this,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                LOCATION_PERMISSION_REQUEST_CODE);
                    } else {
                        getCurrentLocation();
                    }



                }

            }
        });

        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if(task.isComplete()){
                    device_token = task.getResult();


                }
            }
        });


        GetAllEmpData();
    }

    private void getCurrentLocation() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (!isLocationEnabled(Login_User.this)){
            return;
        }

        LocationServices.getFusedLocationProviderClient(Login_User.this)
                .requestLocationUpdates(locationRequest, new LocationCallback() {

                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(getApplicationContext())
                                .removeLocationUpdates(this);
                        if (locationResult != null && locationResult.getLocations().size() > 0) {
                            int latestlocIndex = locationResult.getLocations().size() - 1;
                            double lati = locationResult.getLocations().get(latestlocIndex).getLatitude();
                            double longi = locationResult.getLocations().get(latestlocIndex).getLongitude();
                     //       textLatLong.setText(String.format("Latitude : %s\n Longitude: %s", lati, longi));

                            Log.e("latLong", lati + "--" + longi);

                            Location location = new Location("providerNA");
                            location.setLongitude(longi);
                            location.setLatitude(lati);
                     //       fetchaddressfromlocation(location);





                             for (int i = 0 ; i < employeeModelArrayList.size() ; i++){
                        if(employee_id.getText().toString().equals(employeeModelArrayList.get(i).getEmployee_ID())){
                            if(mPassword.getText().toString().equals(employeeModelArrayList.get(i).getPassword())){

                                mSession.createLoginSession(employeeModelArrayList.get(i).getEmployee_ID(),
                                        employeeModelArrayList.get(i).getEmployee_First_Name(),employeeModelArrayList.get(i).getEmployee_Last_Name(),
                                        employeeModelArrayList.get(i).getEmployee_Email_ID(),
                                        employeeModelArrayList.get(i).getEmployee_Mobile_Number());

                                mSession.setUsertype("USER");


                                if (!mSession.getEmpId().equals(userSessionCheckIn.getUserId())){
                                    userSessionCheckIn.logout();
                                }

                                int finalI = i;
                                firebasing.signInWithEmailAndPassword(employeeModelArrayList.get(i).getEmployee_Email_ID(),"123456")
                                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                            @Override
                                            public void onComplete(@NonNull Task<AuthResult> task) {


                                                if(task.isSuccessful()) {

                                                    HashMap<String,String> h=new HashMap<>();
                                                    h.put("user_id", employeeModelArrayList.get(finalI).getEmployee_ID());
                                                    h.put("user_email", employeeModelArrayList.get(finalI).getEmployee_Email_ID());
                                                    h.put("user_token", device_token);


                                                    ref.child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(h).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            if(task.isSuccessful())
                                                            {
                                                                mSession.setIsLogin(true);
                                                                startActivity(new Intent(Login_User.this, User_MainActivity.class));
                                                                finishAffinity();

                                                            }
                                                            else
                                                            {
                                                                String error=task.getException().getMessage().toString();
                                                                Toast.makeText(Login_User.this,error,Toast.LENGTH_LONG).show();
                                                            }
                                                        }
                                                    });


                                                } else {

                                                    firebasing.createUserWithEmailAndPassword(employeeModelArrayList.get(finalI).getEmployee_Email_ID(),"123456")
                                                            .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                                                @Override
                                                                public void onComplete(@NonNull Task<AuthResult> task) {


                                                                    if(task.isSuccessful()) {

                                                                        HashMap<String,String> h=new HashMap<>();
                                                                        h.put("user_id", employeeModelArrayList.get(finalI).getEmployee_ID());
                                                                        h.put("user_email", employeeModelArrayList.get(finalI).getEmployee_Email_ID());
                                                                        h.put("user_token", device_token);


                                                                        ref.child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(h).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                            @Override
                                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                                if(task.isSuccessful())
                                                                                {
                                                                                    mSession.setIsLogin(true);
                                                                                    startActivity(new Intent(Login_User.this, User_MainActivity.class));
                                                                                    finishAffinity();

                                                                                }
                                                                                else
                                                                                {

                                                                                    String error=task.getException().getMessage().toString();
                                                                                    Toast.makeText(Login_User.this,error,Toast.LENGTH_LONG).show();
                                                                                }
                                                                            }
                                                                        });


                                                                    } else {
                                                                        String message=task.getException().toString();
                                                                        Toast.makeText(Login_User.this,"Error "+message,Toast.LENGTH_LONG).show();
                                                                    }
                                                                }
                                                            });


                                                    String message=task.getException().toString();
                                                    Toast.makeText(Login_User.this,"Error "+message,Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        });


                                Toast.makeText(Login_User.this,"Success...",Toast.LENGTH_SHORT).show();
                                return;
                            }else {
                                Toast.makeText(Login_User.this,"Failed...",Toast.LENGTH_SHORT).show();
                                return;

                            }
                        }
                    }
                    Toast.makeText(Login_User.this,"Employee ID Not Found.....",Toast.LENGTH_SHORT).show();



                        } else {
                    //        progressBar.setVisibility(View.GONE);

                        }
                    }
                }, Looper.getMainLooper());

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            } else {
                Toast.makeText(Login_User.this, "Please allow permission from Setting > App Manager > Hudoor > Permissions", Toast.LENGTH_SHORT).show();
            }
        }
    }

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }




    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(Objects.requireNonNull(Login_User.this))
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        //  Log.e("ResponseSignIn",response.data.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                EmployeeModel employeeModel = new EmployeeModel();
                                employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                employeeModel.setEmployee_Email_ID(object.getString("Employee_Email_ID"));
                                employeeModel.setEmployee_Mobile_Number(object.getString("Employee_Mobile_Number"));
                                employeeModel.setPassword(object.getString("Password"));
                                employeeModelArrayList.add(employeeModel);
                            }


                        } catch (Exception e) {
                            Toast.makeText(Login_User.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(Login_User.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Login_User.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Login_User.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Objects.requireNonNull(Login_User.this)).add(volleyMultipartRequest);
    }



}