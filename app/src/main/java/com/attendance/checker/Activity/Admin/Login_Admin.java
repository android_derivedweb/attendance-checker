package com.attendance.checker.Activity.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Login_Admin extends AppCompatActivity {

    private EditText mPassword, employee_id;
    private ImageView icon_password_visible_signup, icon_password_invisible_signup;
    private TextView header_title;
    private UserSession mSession;
    private ArrayList<EmployeeModel> employeeModelArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark


        mSession = new UserSession(Login_Admin.this);
        employee_id = findViewById(R.id.employee_id);
        mPassword = findViewById(R.id.mPassword);
        icon_password_visible_signup = findViewById(R.id.icon_password_visible_signup);
        icon_password_invisible_signup = findViewById(R.id.icon_password_invisible_signup);
        header_title = findViewById(R.id.header_title);

        header_title.setText("Admin Login");


        findViewById(R.id.backToHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        icon_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_signup.setVisibility(View.GONE);
                icon_password_invisible_signup.setVisibility(View.VISIBLE);


                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_password_invisible_signup.setVisibility(View.GONE);
                icon_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });



        findViewById(R.id.btnLogIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (employee_id.getText().toString().isEmpty()) {
                    Toast.makeText(Login_Admin.this, "Please Your Employee ID", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Login_Admin.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else {

                    for (int i = 0 ; i < employeeModelArrayList.size() ; i++){

                        if(employee_id.getText().toString().equals(employeeModelArrayList.get(i).getEmployee_ID())){
                            if(mPassword.getText().toString().equals(employeeModelArrayList.get(i).getPassword())){

                                if(employeeModelArrayList.get(i).getStatus().equals("Approve")){
                                    mSession.createLoginSession(employeeModelArrayList.get(i).getEmployee_ID(),
                                            employeeModelArrayList.get(i).getEmployee_First_Name(),employeeModelArrayList.get(i).getEmployee_Last_Name(),
                                            employeeModelArrayList.get(i).getEmployee_Email_ID(),
                                            employeeModelArrayList.get(i).getEmployee_Mobile_Number());

                                    mSession.setUsertype("ADMIN");
                                    mSession.setIsLogin(true);

                                    startActivity(new Intent(Login_Admin.this, Admin_MainActivity.class));
                                    finishAffinity();

                                    Toast.makeText(Login_Admin.this,"Success...",Toast.LENGTH_SHORT).show();
                                    return;
                                }else {
                                    Toast.makeText(Login_Admin.this,"Sorry !!! Your Status Not Approved Yet..",Toast.LENGTH_SHORT).show();

                                }


                            }else {
                                Toast.makeText(Login_Admin.this,"Failed...",Toast.LENGTH_SHORT).show();
                                return;

                            }
                        }

                    }

                    Toast.makeText(Login_Admin.this,"Employee ID Not Found.....",Toast.LENGTH_SHORT).show();

                }

            }
        });

        GetAllAdminData();
    }


    private void GetAllAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(Objects.requireNonNull(Login_Admin.this))
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Admin",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Respon",jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Admin");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                EmployeeModel employeeModel = new EmployeeModel();
                                employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                employeeModel.setEmployee_Email_ID(object.getString("Employee_Email_ID"));
                                employeeModel.setEmployee_Mobile_Number(object.getString("Employee_Mobile_Number"));
                                employeeModel.setPassword(object.getString("Password"));
                                employeeModel.setStatus(object.getString("Status"));
                                employeeModelArrayList.add(employeeModel);
                            }


                         
                            
                            

                        } catch (Exception e) {
                            Toast.makeText(Login_Admin.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(Login_Admin.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Login_Admin.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Login_Admin.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                
                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Objects.requireNonNull(Login_Admin.this)).add(volleyMultipartRequest);
    }



}