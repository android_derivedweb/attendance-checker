package com.attendance.checker.Activity.Admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class Register_Admin extends AppCompatActivity {

    private ImageView icon_password_visible_signup, icon_password_invisible_signup, icon_confi_password_visible_signup,
            icon_confi_password_invisible_signup;

    private EditText mEmail, mPassword, mConfirmPassword, first_name, last_name, phoneNumber, employee_id;
    private String lastChar = " ";
    private TextView header_title;

    private static TextView fromDate;
    private static boolean isBirth = false;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private UserSession mSession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        mSession = new UserSession(Register_Admin.this);

        icon_password_visible_signup = findViewById(R.id.icon_password_visible_signup);
        icon_password_invisible_signup = findViewById(R.id.icon_password_invisible_signup);
        icon_confi_password_visible_signup = findViewById(R.id.icon_confi_password_visible_signup);
        icon_confi_password_invisible_signup = findViewById(R.id.icon_confi_password_invisible_signup);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        mConfirmPassword = findViewById(R.id.confirmPassword);
        fromDate = findViewById(R.id.fromDate);
        first_name = findViewById(R.id.first_name);
        last_name = findViewById(R.id.last_name);
        phoneNumber = findViewById(R.id.phoneNumber);
        employee_id = findViewById(R.id.employee_id);
        header_title = findViewById(R.id.header_title);

        header_title.setText("Admin Registration");


        findViewById(R.id.backToHome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        icon_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_signup.setVisibility(View.GONE);
                icon_password_invisible_signup.setVisibility(View.VISIBLE);


                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_password_invisible_signup.setVisibility(View.GONE);
                icon_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });


        icon_confi_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_confi_password_visible_signup.setVisibility(View.GONE);
                icon_confi_password_invisible_signup.setVisibility(View.VISIBLE);


                mConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mConfirmPassword.setSelection(mConfirmPassword.length());

                mConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_confi_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mConfirmPassword.setSelection(mConfirmPassword.length());
                mConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_confi_password_invisible_signup.setVisibility(View.GONE);
                icon_confi_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });


        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getSupportFragmentManager(), "datePicker");
            }
        });


        findViewById(R.id.btnSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //session.createLoginSession();

                if (first_name.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Your First Name", Toast.LENGTH_SHORT).show();
                } else if(employee_id.getText().toString().length() < 5 ){
                    Toast.makeText(Register_Admin.this, "You Need add 5 Digit for Employee Id", Toast.LENGTH_SHORT).show();
                }else if (last_name.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Your Last Name", Toast.LENGTH_SHORT).show();
                } else if (mEmail.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(Register_Admin.this, "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else if (mConfirmPassword.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
                } else if (employee_id.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Your Employee ID", Toast.LENGTH_SHORT).show();
                } else if (!mConfirmPassword.getText().toString().equals(mPassword.getText().toString())) {
                    Toast.makeText(Register_Admin.this, "Password not Match", Toast.LENGTH_SHORT).show();
                } else if (phoneNumber.getText().toString().isEmpty()) {
                    Toast.makeText(Register_Admin.this, "Please Enter Your Phone", Toast.LENGTH_SHORT).show();
                } else if (phoneNumber.getText().toString().length() < 10) {
                    Toast.makeText(Register_Admin.this, "Please Enter Valid Phone!", Toast.LENGTH_SHORT).show();
                } else if (!isBirth) {
                    Toast.makeText(Register_Admin.this, "Please Enter Birth Date", Toast.LENGTH_SHORT).show();
                } else {
                    AddAdminData();
                }
            }
        });

        employee_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if(s.length() == 5) {
                    for (int i = 0; i < mEmployeeIds.size(); i++) {

                        if (s.toString().equals(mEmployeeIds.get(i))) {
                            Toast.makeText(Register_Admin.this, "This Employee id alredy register. Please Use diffrent Employee id...", Toast.LENGTH_LONG).show();
                            employee_id.setText("");
                        }

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        GetAllAdminData();


    }


    private void AddAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(Objects.requireNonNull(Register_Admin.this))
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, mSession.ADD_Admin,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {

                            Log.e("ResponseSignIn",response.data.toString());
                            Alret();

                        } catch (Exception e) {
                            Toast.makeText(Register_Admin.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(Register_Admin.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Register_Admin.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Register_Admin.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id",employee_id.getText().toString());
                params.put("name",first_name.getText().toString());
                params.put("lastname",last_name.getText().toString());
                params.put("email",mEmail.getText().toString());
                params.put("bday",fromDate.getText().toString());
                params.put("number",phoneNumber.getText().toString());
                params.put("password",mPassword.getText().toString());
                params.put("status","Pending");


                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Objects.requireNonNull(Register_Admin.this)).add(volleyMultipartRequest);
    }



    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            fromDate.setText(dateString);
            isBirth = true;

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }

    }

    private ArrayList<String> mEmployeeIds = new ArrayList<>();
    private void GetAllAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(Register_Admin.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Admin",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("ResponseSignIn",response.data.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Admin");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                mEmployeeIds.add(object.getString("Employee_ID"));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(Register_Admin.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(Register_Admin.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(Register_Admin.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(Register_Admin.this).add(volleyMultipartRequest);
    }

    AlertDialog.Builder builder;
    public void Alret(){
        //Uncomment the below code to Set the message and title from the strings.xml file
        //  builder.setMessage(R.string.dialog_message) .setTitle(R.string.dialog_title);
        builder = new AlertDialog.Builder(Register_Admin.this);

        //Setting message manually and performing action on button click
        builder.setMessage("You Need to change status from database to aceess data from admin side \nThank You!!!")
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        finish();
                    }
                });
        //Creating dialog box
        AlertDialog alert = builder.create();
        //Setting the title manually
        alert.setTitle("Alert");
        alert.show();
    }


}