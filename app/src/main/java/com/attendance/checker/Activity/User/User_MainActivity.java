package com.attendance.checker.Activity.User;

import static java.security.AccessController.getContext;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Fragment.User.Frag_Attendance;
import com.attendance.checker.Fragment.User.Frag_Holidays;
import com.attendance.checker.Fragment.User.Frag_Leaves;
import com.attendance.checker.Fragment.User.Frag_MyProfile;
import com.attendance.checker.Fragment.User.Frag_Records;
import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.Model.NotificationModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.Database;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;

public class User_MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4, navLinear5;
    private ImageView navBottomImageHome, navBottomImageCategories, navBottomImageNotifications, navBottomImageWishList, navBottomImageMyStuff;
    private TextView navBottomTxtHome, navBottomTxtCategories, navBottomTxtNotifications, navBottomTxtWishList, navBottomTxtMyStuff;

    private UserSession session;
    private ArrayList<LeaveModel> employeeModelArrayList = new ArrayList<>();
    private ArrayList<LeaveModel> employeeModelArrayList2 = new ArrayList<>();
    private ArrayList<NotificationModel> notificationModelArrayList = new ArrayList<>();

    private ArrayList<String> stringArrayList = new ArrayList<>();
    private ArrayList<String> stringArrayList2 = new ArrayList<>();
    private Database database;

    private final static String default_notification_channel_id = "default" ;


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        session = new UserSession(User_MainActivity.this);

        database = new Database(User_MainActivity.this);
        notificationModelArrayList = database.getAllUser();

        for (int j = 0; j < notificationModelArrayList.size(); j++){
            stringArrayList.add(notificationModelArrayList.get(j).getStart_date());
        }

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        navBottomImageHome = findViewById(R.id.navBottomImageHome);
        navBottomImageCategories = findViewById(R.id.navBottomImageCategories);
        navBottomImageNotifications = findViewById(R.id.navBottomImageNotifications);
        navBottomImageWishList = findViewById(R.id.navBottomImageWishList);
        navBottomImageMyStuff = findViewById(R.id.navBottomImageMyStuff);
        navBottomTxtHome = findViewById(R.id.navBottomTxtHome);
        navBottomTxtCategories = findViewById(R.id.navBottomTxtCategories);
        navBottomTxtNotifications = findViewById(R.id.navBottomTxtNotifications);
        navBottomTxtWishList = findViewById(R.id.navBottomTxtWishList);
        navBottomTxtMyStuff = findViewById(R.id.navBottomTxtMyStuff);


        Frag_Attendance frag_attendance = new Frag_Attendance();
        replaceFragment(R.id.fragmentLinearHome, frag_attendance, "Attendance");

        navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
        navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

        navBottomTxtHome.setTextColor(getResources().getColor(R.color.black));
        navBottomTxtCategories.setTextColor(getResources().getColor(R.color.gray));
        navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.gray));
        navBottomTxtWishList.setTextColor(getResources().getColor(R.color.gray));
        navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.gray));


        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Attendance frag_attendance = new Frag_Attendance();
                replaceFragment(R.id.fragmentLinearHome, frag_attendance, "Attendance");

                navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                navBottomTxtHome.setTextColor(getResources().getColor(R.color.black));
                navBottomTxtCategories.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtWishList.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.gray));

            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Leaves frag_leaves = new Frag_Leaves();
                replaceFragment(R.id.fragmentLinearHome, frag_leaves, "Leaves");

                navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                navBottomTxtHome.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtCategories.setTextColor(getResources().getColor(R.color.black));
                navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtWishList.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.gray));

            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Holidays frag_holidays = new Frag_Holidays();
                replaceFragment(R.id.fragmentLinearHome, frag_holidays, "Holidays");

                navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                navBottomTxtHome.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtCategories.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.black));
                navBottomTxtWishList.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.gray));
            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Records frag_records = new Frag_Records();
                replaceFragment(R.id.fragmentLinearHome, frag_records, "Records");

                navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                navBottomTxtHome.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtCategories.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtWishList.setTextColor(getResources().getColor(R.color.black));
                navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.gray));
            }
        });

        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_MyProfile frag_myProfile = new Frag_MyProfile();
                replaceFragment(R.id.fragmentLinearHome, frag_myProfile, "MyProfile");

                navBottomImageHome.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageCategories.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageNotifications.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageWishList.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                navBottomImageMyStuff.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                navBottomTxtHome.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtCategories.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtNotifications.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtWishList.setTextColor(getResources().getColor(R.color.gray));
                navBottomTxtMyStuff.setTextColor(getResources().getColor(R.color.black));

            }
        });

        //show_Notification();
      //  GetAllEmpData();

    }

    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(User_MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "Leave Application",
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        employeeModelArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseSignIn", jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Leave Application");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (session.getEmpId().equals(object.getString("Employee_ID")) && !object.getString("Authorised_by_Admin").equals("P")) {
                                    LeaveModel employeeModel = new LeaveModel();
                                    employeeModel.setDate_of_Leave_Application(object.getString("Date_of_Leave_Application"));
                                    employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                    employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                    employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                    employeeModel.setType_of_Leave(object.getString("Type_of_Leave"));
                                    employeeModel.setStart_Date_of_Leave(object.getString("Start_Date_of_Leave"));
                                    employeeModel.setEnd_Date_of_Leave(object.getString("End_Date_of_Leave"));
                                    employeeModel.setDuration_of_Leave(object.getString("Duration_of_Leave"));
                                    employeeModel.setBalance_Type_of_Leave_After_Application(object.getString("Balance_Type_of_Leave_After_Application"));
                                    employeeModel.setAuthorised_by_Admin(object.getString("Authorised_by_Admin"));
                                    employeeModelArrayList.add(employeeModel);
                                    stringArrayList2.add(object.getString("Start_Date_of_Leave"));
                                }

                            }

                            if(notificationModelArrayList.isEmpty()){
                                for (int j = 0; j < employeeModelArrayList.size(); j++){

                                    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                                    DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MMM-yy", Locale.ENGLISH);
                                    LocalDate sdate = LocalDate.parse(employeeModelArrayList.get(j).getStart_Date_of_Leave(), inputFormatter);
                                    LocalDate edate = LocalDate.parse(employeeModelArrayList.get(j).getEnd_Date_of_Leave(), inputFormatter);
                                    String mStartDate = outputFormatter.format(sdate);
                                    String mEndDate = outputFormatter.format(edate);
                                    String mFinalDate = mStartDate + " To " +mEndDate;
                                    String mStatus;
                                    if(employeeModelArrayList.get(j).getAuthorised_by_Admin().equals("Y")){
                                        mStatus = "Approved.";
                                    }else {
                                        mStatus = "Rejected.";
                                    }
                                    database.InsertDetails(employeeModelArrayList.get(j).getEmployee_ID(), employeeModelArrayList.get(j).getStart_Date_of_Leave(),
                                            employeeModelArrayList.get(j).getEnd_Date_of_Leave());
                                  //  show_Notification(mFinalDate,mStatus);
                                }
                            } else {
                                ArrayList<String> s1 = new ArrayList<String>(stringArrayList2);
                                s1.removeAll(stringArrayList);
                                for (int j = 0; j < s1.size(); j++){
                                    for (int i = 0; i < employeeModelArrayList.size(); i++){
                                        if(employeeModelArrayList.get(i).getStart_Date_of_Leave().equals(s1.get(j))){

                                            DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                                            DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MMM-yy", Locale.ENGLISH);
                                            LocalDate sdate = LocalDate.parse(employeeModelArrayList.get(j).getStart_Date_of_Leave(), inputFormatter);
                                            LocalDate edate = LocalDate.parse(employeeModelArrayList.get(j).getEnd_Date_of_Leave(), inputFormatter);
                                            String mStartDate = outputFormatter.format(sdate);
                                            String mEndDate = outputFormatter.format(edate);
                                            String mFinalDate = mStartDate + " To " +mEndDate;
                                            String mStatus;
                                            if(employeeModelArrayList.get(j).getAuthorised_by_Admin().equals("Y")){
                                                mStatus = "Approve.";
                                            }else {
                                                mStatus = "Decline.";
                                            }
                                        //    show_Notification(mFinalDate,mStatus);
                                            Toast.makeText(User_MainActivity.this, s1.get(j), Toast.LENGTH_LONG).show();
                                            database.InsertDetails(employeeModelArrayList.get(i).getEmployee_ID(), employeeModelArrayList.get(i).getStart_Date_of_Leave(),
                                                    employeeModelArrayList.get(i).getEnd_Date_of_Leave());
                                        }

                                    }
                                }

                                }

                               // Toast.makeText(User_MainActivity.this, stringArrayList.get(j), Toast.LENGTH_LONG).show();




                        } catch (Exception e) {
                            //           Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show()

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(User_MainActivity.this, "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(User_MainActivity.this, "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(User_MainActivity.this, "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(User_MainActivity.this).add(volleyMultipartRequest);
    }



    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    int i = 0;

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void show_Notification(String text) {

        i++;

        RemoteViews contentView = new RemoteViews(getPackageName() , R.layout.custom_notification_layout ) ;
        contentView.setImageViewResource(R.id.image, R.drawable.app_logo);
        contentView.setTextViewText(R.id.title, "Custom notification");
        contentView.setTextViewText(R.id.text, "This is a custom layout");

        Intent intent = new Intent(getApplicationContext(), User_MainActivity.class);
        String CHANNEL_ID = "MYCHANNEL";


        NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "name", NotificationManager.IMPORTANCE_HIGH);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 1, intent, 0);
        Notification notification = new Notification.Builder(getApplicationContext(), CHANNEL_ID)
                .setContent(contentView)
                .setContentText("Has Been ")
                .setContentTitle("Your Leave Between ")
                .setContentIntent(pendingIntent)
                .setChannelId(CHANNEL_ID)
                .setSmallIcon(R.drawable.app_logo)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.createNotificationChannel(notificationChannel);
        notificationManager.notify(i, notification);


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finishAffinity();
    }
}