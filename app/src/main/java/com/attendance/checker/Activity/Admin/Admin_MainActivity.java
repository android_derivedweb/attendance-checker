package com.attendance.checker.Activity.Admin;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.attendance.checker.Fragment.Admin.Frag_Employee_Registration;
import com.attendance.checker.Fragment.Admin.Frag_Leave;
import com.attendance.checker.Fragment.Admin.Frag_Leave_Status;
import com.attendance.checker.Fragment.Admin.Frag_Record;
import com.attendance.checker.Fragment.User.Frag_Attendance;
import com.attendance.checker.Fragment.User.Frag_Holidays;
import com.attendance.checker.Fragment.User.Frag_Leaves;
import com.attendance.checker.Fragment.User.Frag_Records;
import com.attendance.checker.R;

public class Admin_MainActivity extends AppCompatActivity {

    private LinearLayout navLinear1, navLinear2, navLinear3, navLinear4,navLinear5;
    private ImageView image1, image2, image3, image4,image5;
    private TextView text1, text2, text3, text4,text5;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_main);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);//  set status text dark

        navLinear1 = findViewById(R.id.navLinear1);
        navLinear2 = findViewById(R.id.navLinear2);
        navLinear3 = findViewById(R.id.navLinear3);
        navLinear4 = findViewById(R.id.navLinear4);
        navLinear5 = findViewById(R.id.navLinear5);
        image1 = findViewById(R.id.image1);
        image2 = findViewById(R.id.image2);
        image3 = findViewById(R.id.image3);
        image4 = findViewById(R.id.image4);
        image5 = findViewById(R.id.image5);
        text1 = findViewById(R.id.text1);
        text2 = findViewById(R.id.text2);
        text3 = findViewById(R.id.text3);
        text4 = findViewById(R.id.text4);
        text5 = findViewById(R.id.text5);


        Frag_Leave_Status frag_leave_status = new Frag_Leave_Status();
        replaceFragment(R.id.fragmentLinearHome, frag_leave_status, "leave_status");

        image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
        image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
        image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

        text1.setTextColor(getResources().getColor(R.color.black));
        text2.setTextColor(getResources().getColor(R.color.gray));
        text3.setTextColor(getResources().getColor(R.color.gray));
        text4.setTextColor(getResources().getColor(R.color.gray));
        text5.setTextColor(getResources().getColor(R.color.gray));

        navLinear1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Leave_Status frag_leave_status = new Frag_Leave_Status();
                replaceFragment(R.id.fragmentLinearHome, frag_leave_status, "leave_status");

                image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                text1.setTextColor(getResources().getColor(R.color.black));
                text2.setTextColor(getResources().getColor(R.color.gray));
                text3.setTextColor(getResources().getColor(R.color.gray));
                text4.setTextColor(getResources().getColor(R.color.gray));
                text5.setTextColor(getResources().getColor(R.color.gray));

            }
        });

        navLinear2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Record frag_record = new Frag_Record();
                replaceFragment(R.id.fragmentLinearHome, frag_record, "record_admin");

                image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                text1.setTextColor(getResources().getColor(R.color.gray));
                text2.setTextColor(getResources().getColor(R.color.black));
                text3.setTextColor(getResources().getColor(R.color.gray));
                text4.setTextColor(getResources().getColor(R.color.gray));
                text5.setTextColor(getResources().getColor(R.color.gray));

            }
        });

        navLinear3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Leave frag_leave = new Frag_Leave();
                replaceFragment(R.id.fragmentLinearHome, frag_leave, "Leaves_Admin");

                image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                text1.setTextColor(getResources().getColor(R.color.gray));
                text2.setTextColor(getResources().getColor(R.color.gray));
                text3.setTextColor(getResources().getColor(R.color.black));
                text4.setTextColor(getResources().getColor(R.color.gray));
                text5.setTextColor(getResources().getColor(R.color.gray));
            }
        });

        navLinear4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Holidays frag_holidays = new Frag_Holidays();
                replaceFragment(R.id.fragmentLinearHome, frag_holidays, "Holidays");

                image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));
                image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));

                text1.setTextColor(getResources().getColor(R.color.gray));
                text2.setTextColor(getResources().getColor(R.color.gray));
                text3.setTextColor(getResources().getColor(R.color.gray));
                text4.setTextColor(getResources().getColor(R.color.black));
                text5.setTextColor(getResources().getColor(R.color.gray));
            }
        });


        navLinear5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Frag_Employee_Registration frag_holidays = new Frag_Employee_Registration();
                replaceFragment(R.id.fragmentLinearHome, frag_holidays, "Holidays");

                image1.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image2.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image3.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image4.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.gray));
                image5.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.black));

                text1.setTextColor(getResources().getColor(R.color.gray));
                text2.setTextColor(getResources().getColor(R.color.gray));
                text3.setTextColor(getResources().getColor(R.color.gray));
                text4.setTextColor(getResources().getColor(R.color.gray));
                text5.setTextColor(getResources().getColor(R.color.black));
            }
        });



    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}