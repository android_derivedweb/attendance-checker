package com.attendance.checker.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import com.attendance.checker.Activity.Admin.Admin_MainActivity;
import com.attendance.checker.Activity.User.User_MainActivity;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;

public class Splash extends AppCompatActivity {

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        getWindow().setNavigationBarColor(getResources().getColor(R.color.white));  // for navigation color

        session = new UserSession(Splash.this);


        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {

                if (!session.isLoggedIn()) {
                    startActivity(new Intent(Splash.this, Admin_User_Page.class));
                    finish();
                } else {

                    if (session.getUsertype().equals("ADMIN")) {
                        startActivity(new Intent(Splash.this, Admin_MainActivity.class));
                        finish();
                    } else {
                        startActivity(new Intent(Splash.this, User_MainActivity.class));
                        finish();
                    }


                }
            }
        }, secondsDelayed * 3000);

        RunAnimation();
    }


    private void RunAnimation()
    {
        Animation a = AnimationUtils.loadAnimation(this, R.anim.annimation_text);
        a.reset();
        TextView tv = (TextView) findViewById(R.id.text_anim);
        tv.clearAnimation();
        tv.startAnimation(a);
    }

}