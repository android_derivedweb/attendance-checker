package com.attendance.checker.Fragment.User;

import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class Frag_MyProfile extends Fragment {

    private UserSession session;
    private ArrayList<EmployeeModel> employeeModelArrayList = new ArrayList<>();

    private ImageView editMobile;
    private EditText mobileNo;

    private TextView firstName, lastName, emailId, DOB, eligibleAL, balanceAL, eligibleSL, balanceSL, eligibleUL, balanceUL, eligibleML, balanceML;
    private TextView eligibleHAL, balanceHAL, eligibleHUL, balanceHUL, eligibleWFHL, balanceWFHL, eligibleSPL, balanceSPL;

    public Frag_MyProfile() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_frag_myproflie, container, false);

        session = new UserSession(getContext());

        firstName = view.findViewById(R.id.firstName);
        lastName = view.findViewById(R.id.lastName);
        emailId = view.findViewById(R.id.emailId);
        mobileNo = view.findViewById(R.id.mobileNo);
        DOB = view.findViewById(R.id.DOB);
        eligibleAL = view.findViewById(R.id.eligibleAL);
        balanceAL = view.findViewById(R.id.balanceAL);
        eligibleSL = view.findViewById(R.id.eligibleSL);
        balanceSL = view.findViewById(R.id.balanceSL);
        eligibleUL = view.findViewById(R.id.eligibleUL);
        balanceUL = view.findViewById(R.id.balanceUL);
        eligibleML = view.findViewById(R.id.eligibleML);
        balanceML = view.findViewById(R.id.balanceML);
        editMobile = view.findViewById(R.id.editMobile);

        eligibleHAL = view.findViewById(R.id.eligibleHAL);
        balanceHAL = view.findViewById(R.id.balanceHAL);
        eligibleHUL = view.findViewById(R.id.eligibleHUL);
        balanceHUL = view.findViewById(R.id.balanceHUL);
        eligibleWFHL = view.findViewById(R.id.eligibleWFHL);
        balanceWFHL = view.findViewById(R.id.balanceWFHL);
        eligibleSPL = view.findViewById(R.id.eligibleSPL);
        balanceSPL = view.findViewById(R.id.balanceSPL);


        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });


        view.findViewById(R.id.mChangePassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


        GetAllEmpData();
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);

        editMobile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobileNo.setEnabled(true);

                View v1 = view.findViewById(R.id.mobileNo);
                v1.post(new Runnable() {
                    @Override
                    public void run() {
                        v1.requestFocus();
                        if (imm != null) imm.showSoftInput(v1, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
            }
        });

        mobileNo.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (imm != null) {

                        if (v.getText().toString().length() > 9) {
                            imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.SHOW_IMPLICIT);

                            update_mobile(session.getEmpId(), mobileNo.getText().toString());
                        } else {
                            Toast.makeText(getContext(), "Please enter valid phone!", Toast.LENGTH_SHORT).show();
                        }
                    }

                    return true;
                }
                return false;
            }
        });


        return view;
    }


    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL+ "Employees",
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("ResponseProfile", new String(response.data) + "");
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Employee_ID").equals(session.getEmpId())) {

                                    firstName.setText(object.getString("Employee_First_Name"));
                                    lastName.setText(object.getString("Employee_Last_Name"));

                                    if (object.getString("Employee_Email_ID").equals("")){
                                        emailId.setText("---");
                                    } else {
                                        emailId.setText(object.getString("Employee_Email_ID"));
                                    }

                                    if (object.getString("Employee_Mobile_Number").equals("")){
                                        mobileNo.setText("---");
                                    } else {
                                        mobileNo.setText(object.getString("Employee_Mobile_Number"));
                                    }

                                    if (object.getString("Birthday").equals("")){
                                        DOB.setText("---");
                                    } else {
                                        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                                        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);
                                        LocalDate dateBOD = LocalDate.parse(object.getString("Birthday"), inputFormatter);
                                        DOB.setText(outputFormatter.format(dateBOD));
                                    }


                                    if (object.getString("Eligible_Annual_Leave_(AL)").equals("")){
                                        eligibleAL.setText("0");
                                    } else {
                                        eligibleAL.setText(object.getString("Eligible_Annual_Leave_(AL)"));
                                    }
                                    if (object.getString("Balance_AL").equals("")){
                                        balanceAL.setText("0");
                                    } else {
                                        balanceAL.setText(object.getString("Balance_AL"));
                                    }

                                    if (object.getString("Eligible_Sick_Leave_(SL)").equals("")){
                                        eligibleSL.setText("0");
                                    } else {
                                        eligibleSL.setText(object.getString("Eligible_Sick_Leave_(SL)"));
                                    }
                                    if (object.getString("Balance_SL").equals("")){
                                        balanceSL.setText("0");
                                    } else {
                                        balanceSL.setText(object.getString("Balance_SL"));
                                    }

                                    if (object.getString("Eligible_Unpaid_Leave_(UL)").equals("")){
                                        eligibleUL.setText("0");
                                    } else {
                                        eligibleUL.setText(object.getString("Eligible_Unpaid_Leave_(UL)"));
                                    }
                                    if (object.getString("Balance_UL").equals("")){
                                        balanceUL.setText("0");
                                    } else {
                                        balanceUL.setText(object.getString("Balance_UL"));
                                    }

                                    if (object.getString("Eligible_Maternity_Leave_(ML)").equals("")){
                                        eligibleML.setText("0");
                                    } else {
                                        eligibleML.setText(object.getString("Eligible_Maternity_Leave_(ML)"));
                                    }
                                    if (object.getString("Balance_ML").equals("")){
                                        balanceML.setText("0");
                                    } else {
                                        balanceML.setText(object.getString("Balance_ML"));
                                    }


                                    if (object.getString("Eligible_Half_Day_Annual_Leave_(HAL)").equals("")){
                                        eligibleHAL.setText("0");
                                    } else {
                                        eligibleHAL.setText(object.getString("Eligible_Half_Day_Annual_Leave_(HAL)"));
                                    }
                                    if (object.getString("Balance_HAL").equals("")){
                                        balanceHAL.setText("0");
                                    } else {
                                        balanceHAL.setText(object.getString("Balance_HAL"));
                                    }


                                    if (object.getString("Eligible_Half_Day_Unpaid_Leave_(HUL)").equals("")){
                                        eligibleHUL.setText("0");
                                    } else {
                                        eligibleHUL.setText(object.getString("Eligible_Half_Day_Unpaid_Leave_(HUL)"));
                                    }
                                    if (object.getString("Balance_HUL").equals("")){
                                        balanceHUL.setText("0");
                                    } else {
                                        balanceHUL.setText(object.getString("Balance_HUL"));
                                    }


                                    if (object.getString("Eligible_Working_From_Home_Leave_(WFHL)").equals("")){
                                        eligibleWFHL.setText("0");
                                    } else {
                                        eligibleWFHL.setText(object.getString("Eligible_Working_From_Home_Leave_(WFHL)"));
                                    }
                                    if (object.getString("Balance_WFHL").equals("")){
                                        balanceWFHL.setText("0");
                                    } else {
                                        balanceWFHL.setText(object.getString("Balance_WFHL"));
                                    }


                                    if (object.getString("Eligible_Special_Leave_(SPL)").equals("")){
                                        eligibleSPL.setText("0");
                                    } else {
                                        eligibleSPL.setText(object.getString("Eligible_Special_Leave_(SPL)"));
                                    }
                                    if (object.getString("Balance_SPL").equals("")){
                                        balanceSPL.setText("0");
                                    } else {
                                        balanceSPL.setText(object.getString("Balance_SPL"));
                                    }




                                }

                            }

                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

    private ImageView icon_password_visible_signup, icon_password_invisible_signup, icon_confi_password_visible_signup,
            icon_confi_password_invisible_signup;

    private EditText mPassword, mConfirmPassword;
    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(getActivity());
        dialogForCity.setContentView(R.layout.custom_dailog_employee_password);
        dialogForCity.setCancelable(true);
        dialogForCity.setCanceledOnTouchOutside(true);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        mPassword = dialogForCity.findViewById(R.id.password);
        mConfirmPassword = dialogForCity.findViewById(R.id.confirmPassword);
        icon_password_visible_signup = dialogForCity.findViewById(R.id.icon_password_visible_signup);
        icon_password_invisible_signup = dialogForCity.findViewById(R.id.icon_password_invisible_signup);
        icon_confi_password_visible_signup = dialogForCity.findViewById(R.id.icon_confi_password_visible_signup);
        icon_confi_password_invisible_signup = dialogForCity.findViewById(R.id.icon_confi_password_invisible_signup);

        icon_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_signup.setVisibility(View.GONE);
                icon_password_invisible_signup.setVisibility(View.VISIBLE);


                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_password_invisible_signup.setVisibility(View.GONE);
                icon_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });


        icon_confi_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_confi_password_visible_signup.setVisibility(View.GONE);
                icon_confi_password_invisible_signup.setVisibility(View.VISIBLE);


                mConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mConfirmPassword.setSelection(mConfirmPassword.length());

                mConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_confi_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mConfirmPassword.setSelection(mConfirmPassword.length());
                mConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_confi_password_invisible_signup.setVisibility(View.GONE);
                icon_confi_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });

        dialogForCity.findViewById(R.id.mSubmit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else if (mConfirmPassword.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
                }else if (!mConfirmPassword.getText().toString().equals(mPassword.getText().toString())) {
                    Toast.makeText(getActivity(), "Password not Match", Toast.LENGTH_SHORT).show();
                }else {
                    Update_Password(session.getEmpId(), mConfirmPassword.getText().toString());
                    dialogForCity.dismiss();
                }
            }
        });



        dialogForCity.show();
    }


    private void Update_Password(String mId, String mPassword) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.UPDDATE_PASSWORD,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("ResponseSignIn", response.data.toString());

                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id", mId);
                params.put("password", mPassword);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void update_mobile(String mId, String mPassword) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.UPDATE_MOBILE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("ResponseSignIn", response.data.toString());

                            mobileNo.setEnabled(false);


                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id", mId);
                params.put("mobile", mPassword);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }





}