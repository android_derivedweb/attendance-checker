package com.attendance.checker.Fragment.User;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Adapter.AdapterRecords;
import com.attendance.checker.Model.HolidaysModel;
import com.attendance.checker.Model.RecordsModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Frag_Records extends Fragment {

    private RecyclerView recRecords;
    private AdapterRecords adapterRecords;

    private UserSession session;
    private ArrayList<RecordsModel> recordsModelArrayList = new ArrayList<>();

    private ImageView noData;

    public Frag_Records() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_frag_records, container, false);

        session = new UserSession(getContext());

        noData = view.findViewById(R.id.noData);


        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });


        recRecords = view.findViewById(R.id.recRecords);
        recRecords.setLayoutManager(new LinearLayoutManager(getContext()));
        adapterRecords = new AdapterRecords(getContext(), recordsModelArrayList, new AdapterRecords.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recRecords.setAdapter(adapterRecords);


        getRecords();


        return view;
    }

    private void getRecords() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL+ "Recording Attendance",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("ResponseRecords", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Recording Attendance");
                            for (int i = 0 ; i < mAdminData.length() ; i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (session.getEmpId().equals(object.getString("Employee_ID"))) {
                                    RecordsModel recordsModel = new RecordsModel();
                                    recordsModel.setDate(object.getString("Date"));
                                    recordsModel.setAttendance_In_Time(object.getString("Attendance_In_Time"));
                                    recordsModel.setAttendance_Out_Time(object.getString("Attendance_Out_Time"));
                                    recordsModel.setDuration_of_Time_in_Office(object.getString("Duration_of_Time_in_Office"));
                                    recordsModel.setIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y(object.getString("If_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y"));
                                    recordsModelArrayList.add(recordsModel);
                                }
                            }

                            adapterRecords.notifyDataSetChanged();

                            if (recordsModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recRecords.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                   //         Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                            if (recordsModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recRecords.setVisibility(View.GONE);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


}