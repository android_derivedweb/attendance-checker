package com.attendance.checker.Fragment.User;

import static android.os.Looper.getMainLooper;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Model.RecordsModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.Constants;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.UserSessionCheckIn;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


public class Frag_Attendance extends Fragment {

    private TextView timeTxt, dateTxt, emailId, id, name;

    private UserSession session;
    private UserSessionCheckIn userSessionCheckIn;
    private TextView mCheckIntxt,mCheckOut,mTotalWorking;
    private int Hours;
    private String diff;

    private String date, dateCheckIn;

    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;

    private ArrayList<RecordsModel> recordsModelArrayList = new ArrayList<>();

    private double latitudeDB = 0.0;
    private double longitudeDB = 0.0;

    private boolean isChecked = false;


    public Frag_Attendance() {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_frag_attendance, container, false);

        session = new UserSession(getContext());
        userSessionCheckIn = new UserSessionCheckIn(getContext());

        timeTxt = view.findViewById(R.id.timeTxt);
        dateTxt = view.findViewById(R.id.dateTxt);
        emailId = view.findViewById(R.id.emailId);
        id = view.findViewById(R.id.id);
        name = view.findViewById(R.id.name);
        mCheckIntxt = view.findViewById(R.id.mCheckIn);
        mCheckOut = view.findViewById(R.id.mCheckOut);
        mTotalWorking = view.findViewById(R.id.mTotalWorking);


        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });

        emailId.setText("Email ID : " + session.getEmpEmail());
        id.setText("Employee ID : " + session.getEmpId());
        name.setText("Employee Name : " + session.getEmpName() + " " + session.getEmpLastName());


        if(userSessionCheckIn.getCheckin().equals("")){
            mCheckIntxt.setText("Check IN");
        }else {
            mCheckIntxt.setText(userSessionCheckIn.getCheckin());

        }

        final Handler someHandler = new Handler(getMainLooper());
        someHandler.postDelayed(new Runnable() {
            @Override
            public void run() {

                try {

                    if(!userSessionCheckIn.getCheckin().equals("")){
                        getWorkingTime(userSessionCheckIn.getCheckin(),timeTxt.getText().toString());
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                timeTxt.setText(new SimpleDateFormat("HH:mm:ss", Locale.US).format(new Date()));
                someHandler.postDelayed(this, 1000);
            }
        }, 10);


        date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        String date1 = new SimpleDateFormat("EEEE", Locale.getDefault()).format(new Date());
        dateTxt.setText(date);


        mCheckIntxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dateCheckIn = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());


                for (int i = 0; i < recordsModelArrayList.size(); i++){

                    Log.e("dfdfdff", dateCheckIn + "--" + recordsModelArrayList.get(i).getDate());
                    if (dateCheckIn.equals(recordsModelArrayList.get(i).getDate())){
                        isChecked = true;
                        Toast.makeText(getContext(), "Already checked in today!", Toast.LENGTH_SHORT).show();
                        return;
                    } else {
                        isChecked = false;
                    }

                }

                if (!isLocationEnabled(getContext())) {
                    Toast.makeText(getContext(), "Please, enable location and restart app for better experience!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (!isChecked) {
                    getCurrentLocation("checkIn");
                }

            }
        });


        mCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               getCurrentLocation("checkOut");

            }
        });


        if (isLocationEnabled(getContext())){
            GetAllEmpData();
        } else {
            Toast.makeText(getContext(), "Please, enable location and restart app for better experience!", Toast.LENGTH_SHORT).show();
        }

        getRecords();


        return view;
    }


    private int GetHours(String one,String two){

        try
        {

            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");
            Date Date1 = sdf.parse(one);
            Date Date2 = sdf.parse(two);

            long millse = Date1.getTime() - Date2.getTime();
            long mills = Math.abs(millse);

            int mHours = (int) (mills/(1000 * 60 * 60));
            int Mins = (int) (mills/(1000*60)) % 60;
            int Secs = (int) (mills / 1000) % 60;



            String diff = mHours + ":" + Mins + ":" + Secs; // updated value every1 second
            Log.e("sasas", diff + "");
            return  mHours;

        } catch (Exception e) {

        }


        return 0;

    }

    private void getWorkingTime(String date1 , String date2) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss");


        Date mDate1 = sdf.parse(date1);
        Date mDate2 = sdf.parse(date2);


        long millse = mDate1.getTime() - mDate2.getTime();
        long mills = Math.abs(millse);

        int Hours = (int) (mills/(1000 * 60 * 60));
        int Mins = (int) (mills/(1000*60)) % 60;
        long Secs = (int) (mills / 1000) % 60;

        diff = Hours + ":" + Mins + ":" + Secs; // updated value every1 second
        mTotalWorking.setText("Total Working Hours : "+diff);
    }


    
    private void AddCheckIN(String mDate,
                            String mId,
                            String mName,
                            String mLName,
                            String mCheckIn,
                            String mCheckOut,
                            String mTotal,
                            String mStatus) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

//getting the tag from the edittext

//our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.ADD_CheckIn,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("ResponseSignIn", response.data.toString());

                            userSessionCheckIn.setIsCheckIn(false);
                            userSessionCheckIn.setCheckin("");
                            mCheckIntxt.setText("Check IN");
                            mTotalWorking.setText("Total Working Hours : 00:00:00");

                            Frag_Attendance frag_attendance = new Frag_Attendance();
                            replaceFragment(R.id.fragmentLinearHome, frag_attendance, "Attendance");


                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("current_date", mDate);
                params.put("emp_id", mId);
                params.put("fs_name", mName);
                params.put("ls_name", mLName);
                params.put("check_in", mCheckIn);
                params.put("check_out", mCheckOut);
                params.put("check_total", mTotal);
                params.put("status", mStatus);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
}


    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }


    private void getCurrentLocation(String checkStatus) {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(3000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(getContext(), "Please allow permission from Setting > App Manager > Hudoor > Permissions", Toast.LENGTH_SHORT).show();
            return;
        }

        if (!isLocationEnabled(getContext())){
            return;
        }
        LocationServices.getFusedLocationProviderClient(getActivity())
                .requestLocationUpdates(locationRequest, new LocationCallback() {

                    @Override
                    public void onLocationResult(LocationResult locationResult) {
                        super.onLocationResult(locationResult);
                        LocationServices.getFusedLocationProviderClient(getActivity())
                                .removeLocationUpdates(this);
                        if (locationResult != null && locationResult.getLocations().size() > 0) {
                            int latestlocIndex = locationResult.getLocations().size() - 1;
                            double userLat = locationResult.getLocations().get(latestlocIndex).getLatitude();
                            double userLongi = locationResult.getLocations().get(latestlocIndex).getLongitude();
                            //       textLatLong.setText(String.format("Latitude : %s\n Longitude: %s", lati, longi));

                            Log.e("latLong", userLat + "--" + userLongi);

                            double theta = userLongi - longitudeDB;

                            double dist = Math.sin(deg2rad(userLat))
                                    * Math.sin(deg2rad(latitudeDB))
                                    + Math.cos(deg2rad(userLat))
                                    * Math.cos(deg2rad(latitudeDB))
                                    * Math.cos(deg2rad(theta));
                            dist = Math.acos(dist);
                            dist = rad2deg(dist);
                            dist = dist * 60 * 1.1515 * 1.60934 * 1000;

                          //  Toast.makeText(getContext(), dist + "", Toast.LENGTH_SHORT).show();
                            Log.i("distanceCheck", dist + "--");

                            if (dist < Integer.parseInt(Constants.range)) {

                                if (checkStatus.equals("checkIn")) {

                                    if (userSessionCheckIn.getEmpIscheck()) {
                                        Toast.makeText(getActivity(), "Please Check Out First...", Toast.LENGTH_SHORT).show();
                                    } else {
                                        userSessionCheckIn.setIsCheckIn(true);
                                        userSessionCheckIn.setUserId(session.getEmpId());
                                        userSessionCheckIn.setCheckin(timeTxt.getText().toString());
                                        mCheckIntxt.setText(userSessionCheckIn.getCheckin());

                                        final Handler someHandler = new Handler(getMainLooper());
                                        someHandler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                try {
                                                    getWorkingTime(userSessionCheckIn.getCheckin(), timeTxt.getText().toString());
                                                } catch (ParseException e) {
                                                    e.printStackTrace();
                                                }
                                                someHandler.postDelayed(this, 1000);
                                            }
                                        }, 10);
                                    }
                                    } else {

                                        if(!userSessionCheckIn.getEmpIscheck()){
                                            Toast.makeText(getActivity(),"Please Check IN First...",Toast.LENGTH_SHORT).show();
                                        }else {
                                            String mStatus;
                                            if(GetHours(userSessionCheckIn.getCheckin(), timeTxt.getText().toString()) <= 7){
                                                mStatus = "N";
                                            }else {
                                                mStatus = "Y";
                                            }
                                            date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
                                            AddCheckIN(date, session.getEmpId(), session.getEmpName(), session.getEmpLastName(),
                                                    userSessionCheckIn.getCheckin(), timeTxt.getText().toString(), diff, mStatus);
                                        }

                                    }

                                } else {
                                    Toast.makeText(getContext(), "Please go to office first!!!", Toast.LENGTH_SHORT).show();
                                }

                            /*Location location = new Location("providerNA");
                            location.setLongitude(longi);
                            location.setLatitude(lati);*/
                            //       fetchaddressfromlocation(location);

                        } else {
                            //        progressBar.setVisibility(View.GONE);

                        }
                    }
                }, Looper.getMainLooper());

    }




    /*@Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE && grantResults.length > 0) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            } else {
                Toast.makeText(getContext(), "Please allow permission from Setting > App Manager > Hudoor > Permissions", Toast.LENGTH_SHORT).show();
            }
        }
    }*/

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode != Settings.Secure.LOCATION_MODE_OFF;

        }else{
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }


    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL+ "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("ResponseLocation", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (session.getEmpId().equals(object.getString("Employee_ID"))){

                                    longitudeDB = Double.parseDouble(object.getString("Longitude"));
                                    latitudeDB = Double.parseDouble(object.getString("Latitude"));

                                    Log.e("getLatLonhgg", longitudeDB + "--" + latitudeDB);

                                }

                            }



                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


    private void getRecords() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL+ "Recording Attendance",
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        Log.e("ResponseRecords", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Recording Attendance");
                            for (int i = 0 ; i < mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Employee_ID").equals(session.getEmpId())) {

                                    RecordsModel recordsModel = new RecordsModel();
                                    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                                    DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

                                    LocalDate date = LocalDate.parse(object.getString("Date"), inputFormatter);
                                    String appDate = outputFormatter.format(date);
                                    recordsModel.setDate(appDate);

                                    Log.e("getDatedata", appDate + "");

                                    recordsModel.setAttendance_In_Time(object.getString("Attendance_In_Time"));
                                    recordsModel.setAttendance_Out_Time(object.getString("Attendance_Out_Time"));
                                    recordsModel.setDuration_of_Time_in_Office(object.getString("Duration_of_Time_in_Office"));
                                    recordsModel.setIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y(object.getString("If_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y"));
                                    recordsModelArrayList.add(recordsModel);
                                }

                            }


                        } catch (Exception e) {
                            //         Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }


}