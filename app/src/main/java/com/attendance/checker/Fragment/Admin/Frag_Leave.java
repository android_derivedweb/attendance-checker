package com.attendance.checker.Fragment.Admin;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Adapter.Adapter_admin_record;
import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class Frag_Leave extends Fragment {

    private RecyclerView recRecords;
    private ImageView noData;

    private UserSession session;
    private Adapter_admin_record adapter_admin_record;
    private ArrayList<EmployeeModel> employeeModelArrayList = new ArrayList<>();

    public Frag_Leave() {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.admin_frag_leave, container, false);

        session = new UserSession(getContext());

        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });



        noData = view.findViewById(R.id.noData);
        recRecords = view.findViewById(R.id.recRecords);
        recRecords.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter_admin_record = new Adapter_admin_record(getContext(), employeeModelArrayList, new Adapter_admin_record.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

                Frag_Leaves_inner frag_leaves_inner = new Frag_Leaves_inner(employeeModelArrayList.get(item).getEmployee_ID());
                replaceFragment(R.id.fragmentLinearHome, frag_leaves_inner,"frag_leaves_inner",null);
            }
        });
        recRecords.setAdapter(adapter_admin_record);


        GetAllEmpData();

        return view;


    }

    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL+ "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        employeeModelArrayList.clear();

                        //  Log.e("ResponseSignIn",response.data.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                EmployeeModel employeeModel = new EmployeeModel();
                                employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                employeeModel.setEmployee_Email_ID(object.getString("Employee_Email_ID"));
                                employeeModel.setEmployee_Mobile_Number(object.getString("Employee_Mobile_Number"));
                                employeeModel.setPassword(object.getString("Password"));
                                employeeModelArrayList.add(employeeModel);
                            }


                            adapter_admin_record.notifyDataSetChanged();

                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recRecords.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recRecords.setVisibility(View.VISIBLE);
                            }



                        } catch (Exception e) {
                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recRecords.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recRecords.setVisibility(View.VISIBLE);
                            }

                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }


    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag,
                                   @Nullable String backStackStateName) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(backStackStateName)
                .commit();
    }


}