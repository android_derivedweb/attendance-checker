package com.attendance.checker.Fragment.Admin;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin.Register_Admin;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


public class Frag_Employee_Registration extends Fragment {


    private ImageView icon_password_visible_signup, icon_password_invisible_signup, icon_confi_password_visible_signup,
            icon_confi_password_invisible_signup;

    private EditText mEmail, mPassword, mConfirmPassword, first_name, last_name, phoneNumber, employee_id;
    private String lastChar = " ";
    private TextView header_title;


    private static TextView fromDate;
    private static boolean isBirth = false;
    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    private UserSession mSession;
    private ArrayList<String> mEmployeeIds = new ArrayList<>();

    private RadioButton radio1, radio2;
    private String status = "T";
    private FirebaseAuth firebaseAuth;
    private DatabaseReference ref;


    public Frag_Employee_Registration() {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_register_2, container, false);

        mSession = new UserSession(getActivity());
        icon_password_visible_signup = view.findViewById(R.id.icon_password_visible_signup);
        icon_password_invisible_signup = view.findViewById(R.id.icon_password_invisible_signup);
        icon_confi_password_visible_signup = view.findViewById(R.id.icon_confi_password_visible_signup);
        icon_confi_password_invisible_signup = view.findViewById(R.id.icon_confi_password_invisible_signup);
        mEmail = view.findViewById(R.id.email);
        mPassword = view.findViewById(R.id.password);
        mConfirmPassword = view.findViewById(R.id.confirmPassword);
        fromDate = view.findViewById(R.id.fromDate);
        first_name = view.findViewById(R.id.first_name);
        last_name = view.findViewById(R.id.last_name);
        phoneNumber = view.findViewById(R.id.phoneNumber);
        employee_id = view.findViewById(R.id.employee_id);
        header_title = view.findViewById(R.id.header_title);

        radio1 = view.findViewById(R.id.radio1);
        radio2 = view.findViewById(R.id.radio2);

        header_title.setText("User Registration");

        ref= FirebaseDatabase.getInstance().getReference();
        firebaseAuth =FirebaseAuth.getInstance();

        radio1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1.setChecked(true);
                radio2.setChecked(false);

                status = "T";
            }
        });

        radio2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radio1.setChecked(false);
                radio2.setChecked(true);
                status = "NT";
            }
        });


        icon_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_password_visible_signup.setVisibility(View.GONE);
                icon_password_invisible_signup.setVisibility(View.VISIBLE);


                mPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mPassword.setSelection(mPassword.length());

                mPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mPassword.setSelection(mPassword.length());
                mPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_password_invisible_signup.setVisibility(View.GONE);
                icon_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });


        icon_confi_password_visible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                icon_confi_password_visible_signup.setVisibility(View.GONE);
                icon_confi_password_invisible_signup.setVisibility(View.VISIBLE);


                mConfirmPassword.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                mConfirmPassword.setSelection(mConfirmPassword.length());

                mConfirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
        });

        icon_confi_password_invisible_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT);
                mConfirmPassword.setSelection(mConfirmPassword.length());
                mConfirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());

                icon_confi_password_invisible_signup.setVisibility(View.GONE);
                icon_confi_password_visible_signup.setVisibility(View.VISIBLE);
            }
        });


        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });

    /*    phoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                int digits = phoneNumber.getText().toString().length();
                if (digits > 1)
                    lastChar = phoneNumber.getText().toString().substring(digits-1);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int digits = phoneNumber.getText().toString().length();
                Log.d("LENGTH",""+digits);
                if (!lastChar.equals("-")) {
                    if (digits == 3 || digits == 7) {
                        phoneNumber.append("-");
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });*/


        view.findViewById(R.id.btnSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //session.createLoginSession();

                if (first_name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your First Name", Toast.LENGTH_SHORT).show();
                } else if(employee_id.getText().toString().length() <= 4 ){
                    Toast.makeText(getActivity(), "You Need add 5 Digit for Employee Id", Toast.LENGTH_SHORT).show();
                }else if (last_name.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Last Name", Toast.LENGTH_SHORT).show();
                } else if (mEmail.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Email", Toast.LENGTH_SHORT).show();
                } else if (!mEmail.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(getActivity(), "Invalid email address", Toast.LENGTH_SHORT).show();
                } else if (mPassword.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Password", Toast.LENGTH_SHORT).show();
                } else if (mConfirmPassword.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
                } else if (employee_id.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Your Employee ID", Toast.LENGTH_SHORT).show();
                } else if (!mConfirmPassword.getText().toString().equals(mPassword.getText().toString())) {
                    Toast.makeText(getActivity(), "Password not Match", Toast.LENGTH_SHORT).show();
                } else if (phoneNumber.getText().toString().isEmpty()) {
                    Toast.makeText(getActivity(), "Please Enter Your Phone", Toast.LENGTH_SHORT).show();
                } else if (phoneNumber.getText().toString().length() < 10) {
                    Toast.makeText(getActivity(), "Please Enter Valid Phone!", Toast.LENGTH_SHORT).show();
                } else if (!isBirth) {
                    Toast.makeText(getActivity(), "Please Enter Birth Date", Toast.LENGTH_SHORT).show();
                } else {
                    AddAdminData();
                }

            }
        });

        employee_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                if(s.length() == 5) {
                    for (int i = 0; i < mEmployeeIds.size(); i++) {

                        if (s.toString().equals(mEmployeeIds.get(i))) {
                            Toast.makeText(getActivity(), "This Employee id alredy register. Please Use diffrent Employee id...", Toast.LENGTH_LONG).show();
                            employee_id.setText("");
                        }

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });



        GetAllAdminData();
        return view;


    }



    private void AddAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, mSession.ADD_Employees,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        CreateFirebaseLogin(mEmail.getText().toString(),employee_id.getText().toString(),"null");
                        openDialog();
                        Toast.makeText(getActivity(), "Success", Toast.LENGTH_LONG).show();



                    }
                    },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id",employee_id.getText().toString());
                params.put("name",first_name.getText().toString());
                params.put("lastname",last_name.getText().toString());
                params.put("email",mEmail.getText().toString());
                params.put("bday",fromDate.getText().toString());
                params.put("number",phoneNumber.getText().toString());
                params.put("password",mPassword.getText().toString());
                params.put("status", status);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
            return  dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            fromDate.setText(dateString);
            isBirth = true;

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }

    }


    private void GetAllAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                          Log.e("ResponseSignIn",response.data.toString());
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                mEmployeeIds.add(object.getString("Employee_ID"));

                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }



    private void openDialog() {

        Dialog dialogForCity;
        dialogForCity = new Dialog(getActivity());
        dialogForCity.setContentView(R.layout.custom_dailog_employee_detail);
        dialogForCity.setCancelable(false);
        dialogForCity.setCanceledOnTouchOutside(false);
        dialogForCity.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialogForCity.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);


        TextView emp_name = dialogForCity.findViewById(R.id.emp_name);
        TextView emp_id = dialogForCity.findViewById(R.id.employee_id);
        TextView employee_password = dialogForCity.findViewById(R.id.employee_password);
        emp_name.setText(first_name.getText().toString() + " " + last_name.getText().toString());
        emp_id.setText(employee_id.getText().toString());
        employee_password.setText(mConfirmPassword.getText().toString());

        String mtxtDetails = "Your Hudoor App Login Details Here \n"+first_name.getText().toString() + " " + last_name.getText().toString()+
                "\n Employee ID : "+employee_id.getText().toString()+
                "\n Employee Password : "+mConfirmPassword.getText().toString();



        dialogForCity.findViewById(R.id.whatsapp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.setPackage("com.whatsapp");
                if (intent != null) {
                    intent.putExtra(Intent.EXTRA_TEXT, mtxtDetails);//
                    startActivity(Intent.createChooser(intent, mtxtDetails));
                } else {

                    Toast.makeText(getActivity(), "App not found", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });

        dialogForCity.findViewById(R.id.text_copy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", mtxtDetails);
                clipboard.setPrimaryClip(clip);
                Toast.makeText(dialogForCity.getContext(), "Copied", Toast.LENGTH_SHORT).show();
            }
        });



        dialogForCity.findViewById(R.id.btnSubmitBankDetails).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogForCity.dismiss();

                Frag_Employee_Registration frag_holidays = new Frag_Employee_Registration();
                replaceFragment(R.id.fragmentLinearHome, frag_holidays, "Holidays");

            }
        });



        dialogForCity.show();
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    private void CreateFirebaseLogin(String Email,String user_id,String user_token){
        firebaseAuth.createUserWithEmailAndPassword(Email,"123456")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful())
                        {
                            HashMap<String,String> h=new HashMap<>();
                            h.put("user_id",user_id);
                            h.put("user_email",Email);
                            h.put("user_token",user_token);


                            ref.child("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(h).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    if(task.isSuccessful())
                                    {

                                    }
                                    else
                                    {

                                        String error=task.getException().getMessage().toString();
                                        Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
                                    }
                                }
                            });


                        }
                        else
                        {
                            String error=task.getException().getMessage().toString();
                            Toast.makeText(getActivity(),error,Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }


}