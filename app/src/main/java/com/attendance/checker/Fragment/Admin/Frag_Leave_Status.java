package com.attendance.checker.Fragment.Admin;

import static android.os.Looper.getMainLooper;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin.Admin_MainActivity;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Activity.Splash;
import com.attendance.checker.Activity.User.User_MainActivity;
import com.attendance.checker.Adapter.AdapterLeaves;
import com.attendance.checker.Adapter.AdapterLeaves_status;
import com.attendance.checker.Model.Data;
import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.Model.MyResponse;
import com.attendance.checker.Model.Sender;
import com.attendance.checker.Model.User;
import com.attendance.checker.R;
import com.attendance.checker.Utils.APIService;
import com.attendance.checker.Utils.Client;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import retrofit2.Call;
import retrofit2.Callback;

public class Frag_Leave_Status extends Fragment {


    private UserSession session;
    private ArrayList<LeaveModel> employeeModelArrayList = new ArrayList<>();
    private ImageView noData;
    private RecyclerView recLeaves;
    private AdapterLeaves_status adapterLeaves;
    private String mBalanceLeave = "0";
    private DatabaseReference reference;
    private ArrayList<User> usersList = new ArrayList<>();
    private APIService apiService;


    public Frag_Leave_Status() {

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.admin_leave_status, container, false);

        session = new UserSession(getContext());
        noData = view.findViewById(R.id.noData);

        recLeaves = view.findViewById(R.id.recLeaves);
        apiService = Client.getClient("https://fcm.googleapis.com/").create(APIService.class);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recLeaves.setLayoutManager(linearLayoutManager);
        adapterLeaves = new AdapterLeaves_status(getContext(), employeeModelArrayList, new AdapterLeaves_status.OnItemClickListener() {
            @Override
            public void onItemApprove(int item) {

                DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MMM-yy", Locale.ENGLISH);
                LocalDate sdate = LocalDate.parse(employeeModelArrayList.get(item).getStart_Date_of_Leave(), inputFormatter);
                LocalDate edate = LocalDate.parse(employeeModelArrayList.get(item).getEnd_Date_of_Leave(), inputFormatter);
                String mStartDate = outputFormatter.format(sdate);
                String mEndDate = outputFormatter.format(edate);
                String mFinalDate = mStartDate + " To \n" +mEndDate;


                for (int i = 0 ; i < usersList.size() ; i++){

                    if(usersList.get(i).getUser_id().equals(employeeModelArrayList.get(item).getEmployee_ID())){
                        mSendNotification("Your Leave Between "+mFinalDate + " Has Been Approved",usersList.get(i).getUser_token());
                    }
                }
                Update_Status(employeeModelArrayList.get(item).getEmployee_ID(),
                        employeeModelArrayList.get(item).getStart_Date_of_Leave(), "Y");

                GetLeaveData(employeeModelArrayList.get(item).getEmployee_ID(), employeeModelArrayList.get(item).getType_of_Leave(),
                        employeeModelArrayList.get(item).getDuration_of_Leave());

            }

            @Override
            public void onItemDecline(int item) {

                DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MMM-yy", Locale.ENGLISH);
                LocalDate sdate = LocalDate.parse(employeeModelArrayList.get(item).getStart_Date_of_Leave(), inputFormatter);
                LocalDate edate = LocalDate.parse(employeeModelArrayList.get(item).getEnd_Date_of_Leave(), inputFormatter);
                String mStartDate = outputFormatter.format(sdate);
                String mEndDate = outputFormatter.format(edate);
                String mFinalDate = mStartDate + " To " +mEndDate;


                for (int i = 0 ; i < usersList.size() ; i++){

                    if(usersList.get(i).getUser_id().equals(employeeModelArrayList.get(item).getEmployee_ID())){
                        mSendNotification("Your Leave Between "+mFinalDate + " Has Been Rejected",usersList.get(i).getUser_token());
                    }
                }

                Update_Status(employeeModelArrayList.get(item).getEmployee_ID(),
                        employeeModelArrayList.get(item).getStart_Date_of_Leave(), "N");
            }

        });
        recLeaves.setAdapter(adapterLeaves);


        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });

        GetAllEmpData();
        usersList = new ArrayList<>();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
               // usersList.clear();
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue(User.class);
                    usersList.add(user);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });




        return view;


    }


    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "Leave Application",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        employeeModelArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseSignIn", jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Leave Application");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Authorised_by_Admin").equals("P")) {
                                    LeaveModel employeeModel = new LeaveModel();
                                    employeeModel.setDate_of_Leave_Application(object.getString("Date_of_Leave_Application"));
                                    employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                    employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                    employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                    employeeModel.setType_of_Leave(object.getString("Type_of_Leave"));
                                    employeeModel.setStart_Date_of_Leave(object.getString("Start_Date_of_Leave"));
                                    employeeModel.setEnd_Date_of_Leave(object.getString("End_Date_of_Leave"));
                                    employeeModel.setDuration_of_Leave(object.getString("Duration_of_Leave"));
                                    employeeModel.setBalance_Type_of_Leave_After_Application(object.getString("Balance_Type_of_Leave_After_Application"));
                                    employeeModel.setAuthorised_by_Admin(object.getString("Authorised_by_Admin"));
                                    employeeModelArrayList.add(employeeModel);
                                }

                            }

                            adapterLeaves.notifyDataSetChanged();
                            if (employeeModelArrayList.isEmpty()) {
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }

                        } catch (Exception e) {
                            //           Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                            if (employeeModelArrayList.isEmpty()) {
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void GetLeaveData(String mEmpId, String mType, String mDuration) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, session.BASEURL + "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        employeeModelArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));

                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Employee_ID").equals(mEmpId)) {
                                    Log.e("ResponseSignIn", object.toString());
                                    if (mType.equals("AL")) {
                                        mBalanceLeave = object.getString("Balance_AL");
                                    } else if (mType.equals("ML")) {
                                        mBalanceLeave = object.getString("Balance_ML");
                                    } else if (mType.equals("SL")) {
                                        mBalanceLeave = object.getString("Balance_SL");
                                    } else if (mType.equals("UL")) {
                                        mBalanceLeave = object.getString("Balance_UL");
                                    } else if (mType.equals("HAL")) {
                                        mBalanceLeave = object.getString("Balance_HAL");
                                    } else if (mType.equals("HUL")) {
                                        mBalanceLeave = object.getString("Balance_HUL");
                                    } else if (mType.equals("WFHL")) {
                                        mBalanceLeave = object.getString("Balance_WFHL");
                                    } else if (mType.equals("SPL")) {
                                        mBalanceLeave = object.getString("Balance_SPL");
                                    }

                                    int finalDays = Integer.parseInt(mBalanceLeave) - Integer.parseInt(mDuration);

                                    Log.e("mReminingDays", finalDays + "");

                                    Update_LeavesDays(mEmpId, mType, finalDays + "");
                                }

                            }


                        } catch (Exception e) {
                            //           Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();


                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    private void Update_Status(String mId, String mStartDate, String mStatus) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);

        LocalDate date = LocalDate.parse(mStartDate, inputFormatter);
        String appDate = outputFormatter.format(date);
        Log.e("mData", mId + "---" + appDate + "---" + mStatus);

//our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.UPDDATE_LEAVE,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("ResponseSignIn", response.data.toString());

                            GetAllEmpData();
                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("id", mId);
                params.put("start_date", appDate);
                params.put("status", mStatus);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void Update_LeavesDays(String mId, String type, String duration) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, session.UPDDATE_LEAVEDAYS,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();
                        try {
                            Log.e("ResponseSignIn", response.data.toString());

                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();

                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("id", mId);
                params.put("type", type);
                params.put("days", duration);

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void mSendNotification(String mBody,String token){

        Data data = new Data(mBody);
        Sender sender = new Sender(data, token);

        apiService.sendNotification(sender)
                .enqueue(new Callback<MyResponse>() {
                    @Override
                    public void onResponse(Call<MyResponse> call, retrofit2.Response<MyResponse> response) {
                        if (response.code() == 200){
                            if (response.body().success != 1){
                                //   Toast.makeText(SingleChatMessageActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                            }else {
                                //    Toast.makeText(SingleChatMessageActivity.this, "success!", Toast.LENGTH_SHORT).show();

                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MyResponse> call, Throwable t) {

                    }
                });
    }

}