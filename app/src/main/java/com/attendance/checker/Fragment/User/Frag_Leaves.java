package com.attendance.checker.Fragment.User;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin.Login_Admin;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Adapter.AdapterLeaves;
import com.attendance.checker.Adapter.SelectCitySpinner;
import com.attendance.checker.Model.CityModel;
import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.Model.HolidaysModel;
import com.attendance.checker.Model.LeaveBalanceModel;
import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;


public class Frag_Leaves extends Fragment {

    private RecyclerView recLeaves;
    private AdapterLeaves adapterLeaves;

    private Dialog dialog;

    private static TextView fromDate, toDate;
    private static long mDateDiff;

    private ArrayList<CityModel> mDataCity = new ArrayList<>();
    private String typeStr = "";
    private UserSession mSession;
    private ArrayList<LeaveModel> employeeModelArrayList = new ArrayList<>();
    private ArrayList<LeaveBalanceModel> mBalanceArrayList = new ArrayList<>();
    private String mBalanceSheetDays = "0";

    private ImageView noData;

    private ArrayList<String> mHoliDays = new ArrayList<>();
    private ArrayList<String> mWorkingDays = new ArrayList<>();
    private int mTotalWorkingDays = 0;
    private boolean isAdmin = false;
    private TextView Leave_Balance;

    public Frag_Leaves() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.user_frag_leaves, container, false);

        mSession = new UserSession(getActivity());

        noData = view.findViewById(R.id.noData);

        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSession.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });


        recLeaves = view.findViewById(R.id.recLeaves);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recLeaves.setLayoutManager(linearLayoutManager);
        adapterLeaves = new AdapterLeaves(getContext(), employeeModelArrayList, new AdapterLeaves.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recLeaves.setAdapter(adapterLeaves);


        view.findViewById(R.id.addLeaves).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog();
            }
        });


        GetAllEmpData();
        GetLeaveData();
        getHolidays();
        GetAllAdminData();

        return view;

    }


    private void openDialog() {

        mDataCity.clear();

        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.dialog_date_picker);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Window window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        fromDate = dialog.findViewById(R.id.fromDate);
        toDate = dialog.findViewById(R.id.toDate);
        TextView subMit = dialog.findViewById(R.id.subMit);
        TextView cancel = dialog.findViewById(R.id.cancel);
        Leave_Balance = dialog.findViewById(R.id.Leave_Balance);

        Log.e("fromDate", fromDate.getText().toString() + "--");

        Spinner mCity = dialog.findViewById(R.id.spinnerSelectCity);


        CityModel BatchModel1 = new CityModel();
        BatchModel1.setCityId("AL");
        BatchModel1.setCityname("Eligible Annual Leave (AL)");
        mDataCity.add(BatchModel1);

        CityModel BatchModel2 = new CityModel();
        BatchModel2.setCityId("SL");
        BatchModel2.setCityname("Eligible Sick Leave (SL)");
        mDataCity.add(BatchModel2);

        CityModel BatchModel4 = new CityModel();
        BatchModel4.setCityId("UL");
        BatchModel4.setCityname("Eligible UnPaid (UL)");
        mDataCity.add(BatchModel4);

        CityModel BatchModel5 = new CityModel();
        BatchModel5.setCityId("ML");
        BatchModel5.setCityname("Eligible Maternity Leave (ML)");
        mDataCity.add(BatchModel5);

        CityModel BatchModel6 = new CityModel();
        BatchModel6.setCityId("HAL");
        BatchModel6.setCityname("Eligible Half Day Annual Leave (HAL)");
        mDataCity.add(BatchModel6);

        CityModel BatchModel7 = new CityModel();
        BatchModel7.setCityId("HUL");
        BatchModel7.setCityname("Eligible Half Day Unpaid Leave (HUL)");
        mDataCity.add(BatchModel7);

        CityModel BatchModel8 = new CityModel();
        BatchModel8.setCityId("WFHL");
        BatchModel8.setCityname("Eligible Working From Home Leave (WFHL)");
        mDataCity.add(BatchModel8);

        CityModel BatchModel9 = new CityModel();
        BatchModel9.setCityId("SPL");
        BatchModel9.setCityname("Eligible Special Leave (SPL)");
        mDataCity.add(BatchModel9);

        CityModel BatchModel3 = new CityModel();
        BatchModel3.setCityId("");
        BatchModel3.setCityname("Select Leave Type");
        mDataCity.add(BatchModel3);

        SelectCitySpinner adapter = new SelectCitySpinner(dialog.getContext(),
                android.R.layout.simple_spinner_item, mDataCity);
        adapter.setDropDownViewResource(android.R.layout.simple_list_item_1);
        mCity.setAdapter(adapter);
        mCity.setSelection(adapter.getCount());


        mCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i != mDataCity.size() - 1) {
                    typeStr = mDataCity.get(i).getCityId();

                    switch (typeStr) {
                        case "AL":
                            if (mBalanceArrayList.get(0).getBalanceAnnual_Leave().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceAnnual_Leave();
                            }

                            Leave_Balance.setText("Your available leave balance for AL : " + mBalanceSheetDays);
                            break;
                        case "SL":
                            if (mBalanceArrayList.get(0).getBalanceSick_Leave().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceSick_Leave();
                            }
                            Leave_Balance.setText("Your available leave balance for SL : " + mBalanceSheetDays);
                            break;
                        case "UL":
                            if (mBalanceArrayList.get(0).getBalanceUnpaid_Leave().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceUnpaid_Leave();
                            }
                            Leave_Balance.setText("Your available leave balance for UL : " + mBalanceSheetDays);
                            break;
                        case "ML":
                            if (mBalanceArrayList.get(0).getBalanceMaternity_Leave().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceMaternity_Leave();
                            }
                            Leave_Balance.setText("Your available leave balance for ML : " + mBalanceSheetDays);
                            break;

                        case "HAL":
                            if (mBalanceArrayList.get(0).getBalanceHAL().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceHAL();
                            }
                            Leave_Balance.setText("Your available leave balance for HAL : " + mBalanceSheetDays);
                            break;

                        case "HUL":
                            if (mBalanceArrayList.get(0).getBalanceHUL().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceHUL();
                            }
                            Leave_Balance.setText("Your available leave balance for HUL : " + mBalanceSheetDays);
                            break;

                        case "WFHL":
                            if (mBalanceArrayList.get(0).getBalanceWFHL().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceWFHL();
                            }
                            Leave_Balance.setText("Your available leave balance for WFHL : " + mBalanceSheetDays);
                            break;

                        case "SPL":
                            if (mBalanceArrayList.get(0).getBalanceSPL().equals("")) {
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceSPL();
                            }
                            Leave_Balance.setText("Your available leave balance for SPL : " + mBalanceSheetDays);
                            break;
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new DatePickerFragment();
                newFragment.show(getActivity().getSupportFragmentManager(), "datePicker");
            }
        });
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!fromDate.getText().toString().equals("Select date")) {
                    DialogFragment newFragment = new DatePickerFragment2();
                    newFragment.show(getActivity().getSupportFragmentManager(), "datePicker2");
                } else {
                    Toast.makeText(dialog.getContext(), "Select from date first!", Toast.LENGTH_SHORT).show();
                }

            }
        });


        subMit.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View v) {

                String date = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());

                if (fromDate.getText().toString().equals("Select date")) {
                    Toast.makeText(getContext(), "Please Select Start Date...", Toast.LENGTH_SHORT).show();
                } else if (toDate.getText().toString().equals("Select date")) {
                    Toast.makeText(getContext(), "Please Select End Date...", Toast.LENGTH_SHORT).show();

                } else if (typeStr.equals("")) {
                    Toast.makeText(getContext(), "Please Select Type of Leave...", Toast.LENGTH_SHORT).show();

                } else {

                    switch (typeStr) {
                        case "AL":
                            if (mBalanceArrayList.get(0).getBalanceAnnual_Leave().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceAnnual_Leave();
                            }
                            break;
                        case "SL":
                            if (mBalanceArrayList.get(0).getBalanceSick_Leave().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceSick_Leave();
                            }
                            break;
                        case "UL":
                            if (mBalanceArrayList.get(0).getBalanceUnpaid_Leave().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceUnpaid_Leave();
                            }
                            break;
                        case "ML":
                            if (mBalanceArrayList.get(0).getBalanceMaternity_Leave().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceMaternity_Leave();
                            }
                            break;

                            case "HAL":
                            if (mBalanceArrayList.get(0).getBalanceHAL().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceHAL();
                            }
                            break;

                            case "HUL":
                            if (mBalanceArrayList.get(0).getBalanceHUL().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceHUL();
                            }
                            break;

                            case "WFHL":
                            if (mBalanceArrayList.get(0).getBalanceWFHL().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceWFHL();
                            }
                            break;
                            case "SPL":
                            if (mBalanceArrayList.get(0).getBalanceSPL().equals("")){
                                mBalanceSheetDays = "0";
                            } else {
                                mBalanceSheetDays = mBalanceArrayList.get(0).getBalanceSPL();
                            }
                            break;

                    }

                    Log.e("mData", date + "--"
                            + mSession.getEmpId() + "--"
                            + mSession.getEmpName() + "--"
                            + mSession.getEmpLastName() + "--"
                            + typeStr + "--"
                            + fromDate.getText().toString() + "--"
                            + toDate.getText().toString() + "--"
                            + mDateDiff + "--"
                            + mBalanceSheetDays + "--"
                            + "P" + "--"
                    );


                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = df.parse(fromDate.getText().toString());
                        date2 = df.parse(toDate.getText().toString());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar cal1 = Calendar.getInstance();
                    Calendar cal2 = Calendar.getInstance();
                    cal1.setTime(date1);
                    cal2.setTime(date2);

                    int numberOfDays = 0;
                    while (cal1.before(cal2)) {
                        if ((Calendar.SATURDAY != cal1.get(Calendar.DAY_OF_WEEK))
                                &&(Calendar.SUNDAY != cal1.get(Calendar.DAY_OF_WEEK))) {
                            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                            mWorkingDays.add(dateFormat.format(cal1.getTime()));
                            numberOfDays++;
                        }
                        cal1.add(Calendar.DATE,1);
                    }

                    mTotalWorkingDays = mWorkingDays.size() + 1;

                    for (int i = 0 ; i < mWorkingDays.size() ; i++){


                        for (int j = 0 ; j < mHoliDays.size() ; j++){

                            Log.e("mWorkingDays", mHoliDays.get(j) + "");


                            if(mWorkingDays.get(i).equals(mHoliDays.get(j))){
                                mTotalWorkingDays--;
                                Log.e("mathcjehhdh", "mathcjehhdh");

                            }

                        }
                    }

                    Log.e("getDuff", mDateDiff + "---" + mBalanceSheetDays + "---" + mTotalWorkingDays);

                    if (isAdmin) {

                        if(mDateDiff > Integer.parseInt(mBalanceSheetDays) ) {
                            Toast.makeText(getContext(), "Your Leave days more than Balance Leave...", Toast.LENGTH_SHORT).show();

                        } else {

                            AddAdminData(date, mSession.getEmpId(), mSession.getEmpName(), mSession.getEmpLastName(), typeStr,
                                    fromDate.getText().toString(), toDate.getText().toString(),"" + mDateDiff,
                                    String.valueOf(Integer.parseInt(mBalanceSheetDays) - mDateDiff),"P");

                        }

                    }else {

                        if (mBalanceArrayList.get(0).getStatus().equals("T")) {

                            if (mTotalWorkingDays > Integer.parseInt(mBalanceSheetDays)) {
                                Toast.makeText(getContext(), "Your Leave days more than Balance Leave...", Toast.LENGTH_SHORT).show();
                            } else {
                                AddAdminData(date, mSession.getEmpId(), mSession.getEmpName(), mSession.getEmpLastName(), typeStr,
                                        fromDate.getText().toString(), toDate.getText().toString(), "" + mTotalWorkingDays,
                                        String.valueOf(Integer.parseInt(mBalanceSheetDays) - mTotalWorkingDays), "P");
                            }

                        } else {

                            if(mDateDiff > Integer.parseInt(mBalanceSheetDays) ) {
                                Toast.makeText(getContext(), "Your Leave days more than Balance Leave...", Toast.LENGTH_SHORT).show();
                            } else {
                                AddAdminData(date, mSession.getEmpId(), mSession.getEmpName(), mSession.getEmpLastName(), typeStr,
                                        fromDate.getText().toString(), toDate.getText().toString(),"" + mDateDiff,
                                        String.valueOf(Integer.parseInt(mBalanceSheetDays) - mDateDiff),"P");

                            }

                        }

                    }
                    dialog.dismiss();
                }


            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }


    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMinDate(c.getTimeInMillis());
            return dialog;
        }

        @RequiresApi(api = Build.VERSION_CODES.O)
        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            fromDate.setText(dateString);

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }

    public static class DatePickerFragment2 extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;

            try {
                date = dateFormat.parse(fromDate.getText().toString());
                Log.e("acac", date.toString() + "--");
            } catch (ParseException e) {
                e.printStackTrace();
                Log.e("acac", "error" + "--");

            }

            dialog.getDatePicker().setMinDate(date.getTime());
            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            Calendar calendar = Calendar.getInstance();
            calendar.set(year, month, day);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String dateString = dateFormat.format(calendar.getTime());

            toDate.setText(dateString);

            Date createdConvertedDate = null;
            Date expireCovertedDate = null;
            try {
                createdConvertedDate = dateFormat.parse(fromDate.getText().toString());
                expireCovertedDate = dateFormat.parse(toDate.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar start = new GregorianCalendar();
            start.setTime(createdConvertedDate);

            Calendar end = new GregorianCalendar();
            end.setTime(expireCovertedDate);
            long diff = end.getTimeInMillis() - start.getTimeInMillis();


            mDateDiff = (diff / (24 * 60 * 60 * 1000)) + 1;
            Log.e("acac", diff / (24 * 60 * 60 * 1000) + "--");

           /* if(!time.getText().toString().equals("   Select  Date:")){

                try {
                    Date date2 = new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
                    Date date11=new SimpleDateFormat("dd-MM-yyyy").parse(time.getText().toString());
                    printDifference(date2,date11);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }*/
        }
    }


    private void GetAllEmpData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL + "Leave Application",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        employeeModelArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseSignIn", jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Leave Application");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (mSession.getEmpId().equals(object.getString("Employee_ID"))) {
                                    LeaveModel employeeModel = new LeaveModel();
                                    employeeModel.setDate_of_Leave_Application(object.getString("Date_of_Leave_Application"));
                                    employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                    employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                    employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                    employeeModel.setType_of_Leave(object.getString("Type_of_Leave"));
                                    employeeModel.setStart_Date_of_Leave(object.getString("Start_Date_of_Leave"));
                                    employeeModel.setEnd_Date_of_Leave(object.getString("End_Date_of_Leave"));
                                    employeeModel.setDuration_of_Leave(object.getString("Duration_of_Leave"));
                                    employeeModel.setBalance_Type_of_Leave_After_Application(object.getString("Balance_Type_of_Leave_After_Application"));
                                    employeeModel.setAuthorised_by_Admin(object.getString("Authorised_by_Admin"));
                                    employeeModelArrayList.add(employeeModel);
                                }

                            }

                            adapterLeaves.notifyDataSetChanged();

                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                 //           Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void GetLeaveData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL + "Employees",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        mBalanceArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Employees");


                            Log.e("ResponseLeave", jsonObject.toString());

                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Employee_ID").equals(mSession.getEmpId())) {


                                    LeaveBalanceModel employeeModel = new LeaveBalanceModel();
                                    employeeModel.setTotalAnnual_Leave(object.getString("Eligible_Annual_Leave_(AL)"));

                                    employeeModel.setBalanceAnnual_Leave(object.getString("Balance_AL"));
                                    employeeModel.setTotalSick_Leave(object.getString("Eligible_Sick_Leave_(SL)"));
                                    employeeModel.setBalanceSick_Leave(object.getString("Balance_SL"));
                                    employeeModel.setTotalUnpaid_Leave(object.getString("Eligible_Unpaid_Leave_(UL)"));
                                    employeeModel.setBalanceUnpaid_Leave(object.getString("Balance_UL"));
                                    employeeModel.setTotalMaternity_Leave(object.getString("Eligible_Maternity_Leave_(ML)"));
                                    employeeModel.setBalanceMaternity_Leave(object.getString("Balance_ML"));

                                    employeeModel.setTotalHAL(object.getString("Eligible_Half_Day_Annual_Leave_(HAL)"));
                                    employeeModel.setBalanceHAL(object.getString("Balance_HAL"));

                                    employeeModel.setTotalHUL(object.getString("Eligible_Half_Day_Unpaid_Leave_(HUL)"));
                                    employeeModel.setBalanceHUL(object.getString("Balance_HUL"));

                                    employeeModel.setTotalWFHL(object.getString("Eligible_Working_From_Home_Leave_(WFHL)"));
                                    employeeModel.setBalanceWFHL(object.getString("Balance_WFHL"));

                                    employeeModel.setTotalSPL(object.getString("Eligible_Special_Leave_(SPL)"));
                                    employeeModel.setBalanceSPL(object.getString("Balance_SPL"));


                                    employeeModel.setStatus(object.getString("Teacher_(T)_-_Non_Teacher_(NT)"));

                                    mBalanceArrayList.add(employeeModel);
                                }
                            }


                        } catch (Exception e) {
                //            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void AddAdminData(String mDate,
                              String mId,
                              String mName,
                              String mLName,
                              String mLeaveType,
                              String mSDate,
                              String mLDate,
                              String mDiff,
                              String mRemaingDays,
                              String mStatus) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, mSession.ADD_Leaves,
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            Log.e("ResponseSignInAddData", new String(response.data));
                            GetLeaveData();
                            GetAllEmpData();
                            Toast.makeText(getActivity(), "Success...", Toast.LENGTH_SHORT).show();


                            noData.setVisibility(View.GONE);
                            recLeaves.setVisibility(View.VISIBLE);
                            mWorkingDays.clear();
                         //   mWorkingDays.clear();


                        } catch (Exception e) {
                            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                params.put("current_date", mDate);
                params.put("emp_id", mId);
                params.put("fs_name", mName);
                params.put("ls_name", mLName);
                params.put("leave_type", mLeaveType);
                params.put("s_date", mSDate);
                params.put("e_date", mLDate);
                params.put("diff_date", mDiff);
                params.put("bl_days", mRemaingDays);
                params.put("status", mStatus);

                Log.e("dfffdf", mDate + "--" + mId + "--" + mName + "--" + mLName + "--" + mLeaveType + "--" + mSDate + "--" + mLDate + "--" + mDiff + "--" +
                        mRemaingDays + "--" + mStatus);

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


    private void getHolidays() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Holiday Table",
                new Response.Listener<NetworkResponse>() {
                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
                        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);


                        Log.e("ResponseHolid", new String(response.data));
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            JSONArray mAdminData = jsonObject.getJSONArray("Holiday Table");
                            for (int i = 0 ; i < mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                LocalDate dateA = LocalDate.parse(object.getString("Date"), inputFormatter);
                                String appDate = outputFormatter.format(dateA);

                                mHoliDays.add(appDate);

                                Log.e("getDateFormnat", appDate + "");

                            }

                        } catch (Exception e) {
                            //        Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }

    private void GetAllAdminData() {
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL+ "Admin",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();


                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("Respon",jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Admin");
                            for (int i = 0 ; i<mAdminData.length() ; i++){
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (object.getString("Employee_ID").equals(mSession.getEmpId())){
                                    isAdmin = true;
                                    break;
                                }


                            }


                        } catch (Exception e) {
                            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getContext(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getContext(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getContext(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getContext()).add(volleyMultipartRequest);
    }



}