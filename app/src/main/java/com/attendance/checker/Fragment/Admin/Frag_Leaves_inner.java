package com.attendance.checker.Fragment.Admin;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;

import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.attendance.checker.Activity.Admin_User_Page;
import com.attendance.checker.Adapter.AdapterLeaves;
import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.R;
import com.attendance.checker.Utils.UserSession;
import com.attendance.checker.Utils.VolleyMultipartRequest;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.HashMap;
import java.util.Map;


public class Frag_Leaves_inner extends Fragment {

    private RecyclerView recLeaves;
    private AdapterLeaves adapterLeaves;

    private UserSession mSession;
    private ArrayList<LeaveModel> employeeModelArrayList = new ArrayList<>();

    private ImageView noData;

    private String empID = "";

    public Frag_Leaves_inner(String empID) {
        this.empID = empID;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.frag_admin_leaves_inner, container, false);

        mSession = new UserSession(getActivity());

        view.findViewById(R.id.btnSignOut).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSession.logout();
                startActivity(new Intent(getContext(), Admin_User_Page.class));
                requireActivity().finishAffinity();
            }
        });


        noData = view.findViewById(R.id.noData);


        recLeaves = view.findViewById(R.id.recLeaves);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recLeaves.setLayoutManager(linearLayoutManager);
        adapterLeaves = new AdapterLeaves(getContext(), employeeModelArrayList, new AdapterLeaves.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {

            }
        });
        recLeaves.setAdapter(adapterLeaves);


        GetAllEmpData(empID);

        return view;
    }


    private void GetAllEmpData(String empID) {
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        //our custom volley request
        VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.GET, mSession.BASEURL + "Leave Application",
                new Response.Listener<NetworkResponse>() {
                    @Override
                    public void onResponse(NetworkResponse response) {

                        progressDialog.dismiss();

                        employeeModelArrayList.clear();
                        try {
                            JSONObject jsonObject = new JSONObject(new String(response.data));
                            Log.e("ResponseSignIn", jsonObject.toString());
                            JSONArray mAdminData = jsonObject.getJSONArray("Leave Application");
                            for (int i = 0; i < mAdminData.length(); i++) {
                                JSONObject object = mAdminData.getJSONObject(i);

                                if (empID.equals(object.getString("Employee_ID"))) {
                                    LeaveModel employeeModel = new LeaveModel();
                                    employeeModel.setDate_of_Leave_Application(object.getString("Date_of_Leave_Application"));
                                    employeeModel.setEmployee_ID(object.getString("Employee_ID"));
                                    employeeModel.setEmployee_First_Name(object.getString("Employee_First_Name"));
                                    employeeModel.setEmployee_Last_Name(object.getString("Employee_Last_Name"));
                                    employeeModel.setType_of_Leave(object.getString("Type_of_Leave"));
                                    employeeModel.setStart_Date_of_Leave(object.getString("Start_Date_of_Leave"));
                                    employeeModel.setEnd_Date_of_Leave(object.getString("End_Date_of_Leave"));
                                    employeeModel.setDuration_of_Leave(object.getString("Duration_of_Leave"));
                                    employeeModel.setBalance_Type_of_Leave_After_Application(object.getString("Balance_Type_of_Leave_After_Application"));
                                    employeeModel.setAuthorised_by_Admin(object.getString("Authorised_by_Admin"));
                                    employeeModelArrayList.add(employeeModel);
                                }

                            }

                            adapterLeaves.notifyDataSetChanged();


                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }
                        } catch (Exception e) {
                 //           Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                            if (employeeModelArrayList.isEmpty()){
                                noData.setVisibility(View.VISIBLE);
                                recLeaves.setVisibility(View.GONE);
                            } else {
                                noData.setVisibility(View.GONE);
                                recLeaves.setVisibility(View.VISIBLE);
                            }

                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        if (error instanceof ServerError)
                            Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_LONG).show();
                        else if (error instanceof TimeoutError)
                            Toast.makeText(getActivity(), "Connection Timed Out", Toast.LENGTH_LONG).show();
                        else if (error instanceof NetworkError)
                            Toast.makeText(getActivity(), "Bad Network Connection", Toast.LENGTH_LONG).show();
                    }
                }) {

            /*
             * If you want to add more parameters with the image
             * you can do it here
             * here we have only one parameter with the image
             * which is tags
             * */
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();

                return params;
            }


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<String, String>();
                params.put("Accept", "application/json");
                // params.put("Authorization", "Bearer " + session.getAPIToken());
                return params;
            }

            /*
             * Here we are passing image by renaming it with a unique name
             * */
            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();

                return params;
            }
        };
        //adding the request to volley
        Volley.newRequestQueue(getActivity()).add(volleyMultipartRequest);
    }


}