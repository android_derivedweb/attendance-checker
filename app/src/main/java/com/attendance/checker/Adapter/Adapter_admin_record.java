package com.attendance.checker.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.checker.Model.EmployeeModel;
import com.attendance.checker.Model.HolidaysModel;
import com.attendance.checker.R;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;


public class Adapter_admin_record extends RecyclerView.Adapter<Adapter_admin_record.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<EmployeeModel> employeeModelArrayList;


    public Adapter_admin_record(Context mContext, ArrayList<EmployeeModel> employeeModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.employeeModelArrayList = employeeModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_admin_records, parent, false);
        return new Viewholder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {


        holder.setIsRecyclable(false);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
                notifyDataSetChanged();
            }
        });

        holder.name.setText(employeeModelArrayList.get(position).getEmployee_First_Name() + " " + employeeModelArrayList.get(position).getEmployee_Last_Name());
        holder.idEmp.setText(employeeModelArrayList.get(position).getEmployee_ID());
        holder.email.setText(employeeModelArrayList.get(position).getEmployee_Email_ID());

    /*    DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

        LocalDate date = LocalDate.parse(holidaysModelArrayList.get(position).getDate(), inputFormatter);
        String appDate = outputFormatter.format(date);

        holder.date.setText(appDate);
        holder.description.setText(holidaysModelArrayList.get(position).getDescription());*/


        if (!((position % 2) == 1)){
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.light_gray));
        }

    }

    @Override
    public int getItemCount() {
        return employeeModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView name, idEmp, email;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            idEmp = itemView.findViewById(R.id.idEmp);
            email = itemView.findViewById(R.id.email);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
