package com.attendance.checker.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.R;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;


public class AdapterLeaves extends RecyclerView.Adapter<AdapterLeaves.Viewholder> {

    private final OnItemClickListener listener;
    private final ArrayList<LeaveModel> employeeModelArrayList;
    private Context mContext;


    public AdapterLeaves(Context mContext, ArrayList<LeaveModel> employeeModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.employeeModelArrayList = employeeModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_leaves, parent, false);
        return new Viewholder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
                notifyDataSetChanged();
            }
        });

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

        LocalDate dateA = LocalDate.parse(employeeModelArrayList.get(position).getDate_of_Leave_Application(), inputFormatter);
        String appDate = outputFormatter.format(dateA);

        LocalDate dateS = LocalDate.parse(employeeModelArrayList.get(position).getStart_Date_of_Leave(), inputFormatter);
        String startDate = outputFormatter.format(dateS);

        LocalDate dateE = LocalDate.parse(employeeModelArrayList.get(position).getEnd_Date_of_Leave(), inputFormatter);
        String endDate = outputFormatter.format(dateE);


        holder.app_date.setText(appDate);
        holder.start_date.setText(startDate);
        holder.end_date.setText(endDate);
        holder.leave_type.setText(employeeModelArrayList.get(position).getType_of_Leave());
        holder.duration.setText(employeeModelArrayList.get(position).getDuration_of_Leave());
        holder.status.setText(employeeModelArrayList.get(position).getAuthorised_by_Admin());

        holder.app_date.setSelected(true);
        holder.start_date.setSelected(true);
        holder.end_date.setSelected(true);
        holder.leave_type.setSelected(true);
        holder.duration.setSelected(true);
        holder.status.setSelected(true);

        switch (employeeModelArrayList.get(position).getAuthorised_by_Admin()) {
            case "Y":
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.green_light));
                break;
            case "N":
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.red_light));
                break;
            case "P":
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.yellow_light));
                break;
        }

    }

    @Override
    public int getItemCount() {
        return employeeModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView app_date,start_date,end_date,leave_type,duration,status;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            app_date = itemView.findViewById(R.id.app_date);
            start_date = itemView.findViewById(R.id.start_date);
            end_date = itemView.findViewById(R.id.end_date);
            leave_type = itemView.findViewById(R.id.leave_type);
            duration = itemView.findViewById(R.id.duration);
            status = itemView.findViewById(R.id.status);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }
}
