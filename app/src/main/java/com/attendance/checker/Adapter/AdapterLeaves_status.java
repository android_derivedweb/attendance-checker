package com.attendance.checker.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.R;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;


public class AdapterLeaves_status extends RecyclerView.Adapter<AdapterLeaves_status.Viewholder> {

    private final OnItemClickListener listener;
    private final ArrayList<LeaveModel> employeeModelArrayList;
    private Context mContext;


    public AdapterLeaves_status(Context mContext, ArrayList<LeaveModel> employeeModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.employeeModelArrayList = employeeModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_leaves_status, parent, false);
        return new Viewholder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {



        holder.mApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemApprove(position);
                notifyDataSetChanged();
            }
        });
        holder.mDecline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemDecline(position);
                notifyDataSetChanged();
            }
        });

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);


        LocalDate dateS = LocalDate.parse(employeeModelArrayList.get(position).getStart_Date_of_Leave(), inputFormatter);
        String startDate = outputFormatter.format(dateS);

        LocalDate dateE = LocalDate.parse(employeeModelArrayList.get(position).getEnd_Date_of_Leave(), inputFormatter);
        String endDate = outputFormatter.format(dateE);


        holder.mEmp_ID.setText(employeeModelArrayList.get(position).getEmployee_ID() );
        holder.mEmp_Duration.setText(employeeModelArrayList.get(position).getDuration_of_Leave() + " Days");
        holder.mEmp_Name.setText(employeeModelArrayList.get(position).getEmployee_First_Name() + " " + employeeModelArrayList.get(position).getEmployee_Last_Name());
        holder.mEmp_Date.setText(startDate + " - " + endDate);



    }

    @Override
    public int getItemCount() {
        return employeeModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView mEmp_ID;
        TextView mEmp_Name;
        TextView mEmp_Date;
        TextView mApprove;
        TextView mDecline;
        TextView mEmp_Duration;
        public Viewholder(@NonNull View itemView) {
            super(itemView);
            mEmp_ID = itemView.findViewById(R.id.mEmp_ID);
            mEmp_Name = itemView.findViewById(R.id.mEmp_Name);
            mEmp_Date = itemView.findViewById(R.id.mEmp_Date);
            mApprove = itemView.findViewById(R.id.mApprove);
            mDecline = itemView.findViewById(R.id.mDecline);
            mEmp_Duration = itemView.findViewById(R.id.mEmp_Duration);
        }
    }

    public interface OnItemClickListener {
        void onItemApprove(int item);
        void onItemDecline(int item);
    }
}
