package com.attendance.checker.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.checker.Model.HolidaysModel;
import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.R;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;


public class AdapterHolidays extends RecyclerView.Adapter<AdapterHolidays.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<HolidaysModel> holidaysModelArrayList;


    public AdapterHolidays(Context mContext, ArrayList<HolidaysModel> holidaysModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.holidaysModelArrayList = holidaysModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_holidays, parent, false);
        return new Viewholder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {

        holder.setIsRecyclable(false);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
                notifyDataSetChanged();
            }
        });

        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

        LocalDate date = LocalDate.parse(holidaysModelArrayList.get(position).getDate(), inputFormatter);
        String appDate = outputFormatter.format(date);

        holder.date.setText(appDate);
        holder.description.setText(holidaysModelArrayList.get(position).getDescription());


        if (!((position % 2) == 1)){
            holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.light_gray));
        }

    }

    @Override
    public int getItemCount() {
        return holidaysModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView date, description;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            date = itemView.findViewById(R.id.date);
            description = itemView.findViewById(R.id.description);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }


}
