package com.attendance.checker.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.attendance.checker.Model.LeaveModel;
import com.attendance.checker.Model.RecordsModel;
import com.attendance.checker.R;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Locale;


public class AdapterRecords extends RecyclerView.Adapter<AdapterRecords.Viewholder> {

    private final OnItemClickListener listener;
    private Context mContext;
    private ArrayList<RecordsModel> recordsModelArrayList;


    public AdapterRecords(Context mContext, ArrayList<RecordsModel> recordsModelArrayList, OnItemClickListener listener) {
        this.mContext = mContext;
        this.listener = listener;
        this.recordsModelArrayList = recordsModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.adapter_records, parent, false);
        return new Viewholder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, @SuppressLint("RecyclerView") int position) {



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClick(position);
                notifyDataSetChanged();
            }
        });



        DateTimeFormatter inputFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        DateTimeFormatter outputFormatter = DateTimeFormatter.ofPattern("dd-MM-yyy", Locale.ENGLISH);

        LocalDate date = LocalDate.parse(recordsModelArrayList.get(position).getDate(), inputFormatter);
        String appDate = outputFormatter.format(date);
        holder.date.setText(appDate);


        holder.inTime.setText(recordsModelArrayList.get(position).getAttendance_In_Time());
        holder.outTime.setText(recordsModelArrayList.get(position).getAttendance_Out_Time());
        holder.duration.setText(recordsModelArrayList.get(position).getDuration_of_Time_in_Office());

        holder.status.setText(recordsModelArrayList.get(position).getIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y());


        switch (recordsModelArrayList.get(position).getIf_Less_than_7_Hours_and_Within_Time_Range_Then_N_else_Y()) {
            case "N":
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.red_light));
                break;
            case "Y":
                holder.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.green_light));
                break;
        }

    }




    @Override
    public int getItemCount() {
        return recordsModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView date, inTime, outTime, duration, status;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            inTime = itemView.findViewById(R.id.inTime);
            outTime = itemView.findViewById(R.id.outTime);
            duration = itemView.findViewById(R.id.duration);
            status = itemView.findViewById(R.id.status);
            date = itemView.findViewById(R.id.date);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}
